#!/usr/bin/env python3
__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
 tool generaring and evaluating HPAR for CosmoFlow
 
save: hpar_XX.conf.yaml and hpar_XX.sum.yaml 

uses generator for input

Segments:
* gen_CNN_block
* gen_FC_block
* gen_train_block
* build_model
* eval_speed

****Run on Cori
   module load tensorflow/gpu-2.1.0-py37	

One worker:
it may need   export CUDA_VISIBLE_DEVICES=2 
./genHParEval_Cosmo.py

on GPUs
salloc  --gres=gpu:8  --ntasks-per-node=8  --exclusive -C gpu  -t 3:59:00 
srun bash -c '  nvidia-smi '
srun -n1 bash -c ' python -u ./genHPar_Cosmo.py'

'''

import socket  # for hostname
import os, time
import secrets
import warnings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings
#import socket  # for hostname

from pprint import pprint
startT0 = time.time()
import numpy as np

from tensorflow.keras.layers import Dense, Dropout,   Input, Conv3D,MaxPool3D,Flatten,Reshape, Lambda, BatchNormalization,AveragePooling3D
import tensorflow as tf
from tensorflow.python.keras import backend as K  # just for GPU count

print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))
import sys,os
sys.path.append(os.path.abspath("toolbox")) # add ln -s for interactive testing
from Util_IOfunc import  write_yaml, read_yaml
from Util_Func import build_model_cosmo7


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--genConf', default='cosmo_hpo.conf.yaml',help="defines all contrants for HPAR generation and evaluation")
    parser.add_argument("-v","--verb",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("-o","--outPath",default='out',help="output path for HPAR")
    parser.add_argument("-n","--numTrials",default=5, type=int,help="number of HPAR sets to be generated")

    args = parser.parse_args()
    args.epochs=2
    args.steps=8
    args.facility='summit'
    np.random.seed() #  set the seed to a random number obtained from /dev/urandom 
    for i in range(int(50*np.random.rand())):
        np.random.rand()  # now rnd is burned in

    for arg in vars(args):  
        print( 'myArg:',arg, getattr(args, arg))
    print('seed test:',np.random.rand(4))

    return args


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class HparGenerator_CosmoFlow():
    def __init__(self,conf,verb=1):
        self.myConf=conf
        self.wrkLyr=Input(shape=tuple(conf['inpShape']),name='inp')
        self.verb=verb

        if verb: print('HypGen inp layer shape',self.wrkLyr.get_shape(),'verb=',verb)
        self.proposal={'inpShape':conf['inpShape']} # generated HPAR configuration
        self.proposal['outShape']=conf['outShape']
        self.proposal['myId']=secrets.token_hex(nbytes=4)
        self.timeBegin = time.time()
        self.qa={}
        self.proposal['hpo_qa_1gpu']=self.qa # this is a shortcut

    #...!...!....................
    def prep_CNN_block(self,block):
        if self.verb: print('CNN_block %s start'%block)
        C=self.myConf['cnn_block']
        if self.verb>1: pprint(C)
        numLyr=int(np.random.choice(C['num_layer']))
        if self.verb>1: print(block,'numLyr=',numLyr)
        lyr=self.wrkLyr
        kernel=int(np.random.choice(C['kernel']))
        cnn_act=str(np.random.choice(C['activation']))
        pool_type=str(np.random.choice(C['pool_type']))
        pool_size=int(np.random.choice(C['pool_size']))
        strides=int(np.random.choice(C['strides']))
        cnn_pad='same'
        propBlock={'kernel':kernel,'strides':strides,'activation':cnn_act,'padding':cnn_pad,'pool_type':pool_type, 'pool_size':pool_size}
        if self.verb>1: print('com prop block',propBlock)
        propL=[]
        for k in range(numLyr):
            if lyr.get_shape()[-2]==1: break # nothing to reduce
            filt=lyr.get_shape()[-1]*int(np.random.choice(C['dim_fact']))
            #print('lyr=%d cnn filt=%d'%(k,filt),pool_type,'avr','pool_type'=='avr',type(pool_type))
            
            try:
                lyr=Conv3D(filt,kernel,strides=strides,activation=cnn_act,padding=cnn_pad )(lyr)
                if pool_type=='avr':
                    lyr= AveragePooling3D(pool_size=pool_size,padding=cnn_pad)(lyr)
                elif pool_type=='max':
                    lyr= MaxPool3D(pool_size=pool_size,padding=cnn_pad)(lyr)
                else: crash_789
            except:
                if self.verb>1: print('create Conv3D+Pool layer failed',k,block)
                return False
            if self.verb>1: print('layer=%d Conv3D+Pool, output:'%k,lyr.get_shape())
            propL.append(filt)

        if self.verb>0: print('genCNN_%s success,last layer shape'%block,self.wrkLyr.get_shape(),'filters:',propL)
        propBlock['filters']=propL
        self.proposal['cnn_block_'+block]=propBlock

        self.wrkLyr=lyr
        return True

        
    #...!...!....................
    def prep_FC_block(self):
        if self.verb>0: print('FC_block start')
        C=self.myConf['fc']
        if self.verb>1: pprint(C)        
        numLyr=int(np.random.choice(C['num_layer']))

        if self.verb>1: print('numLyr=',numLyr)
        fc_act=str(np.random.choice(C['activation']))
        dropFrac=float(np.random.choice(C['dropFrac']))

        # generate params in the reverse order, no need to check if 'buildable'
        filt1=int(np.random.choice(C['last_dim']))
        propL=[filt1]
        for i in range(1,numLyr):
            fact=np.random.choice(C['dim_fact'])
            filt1=int(min(512,filt1*fact))
            propL.insert(0,filt1)

        if self.verb>1: 
            print('genFC success, proposal:')
            pprint(propL)
        self.proposal['fc_block']={'dropFrac':dropFrac,'activation':fc_act,'units':propL}
        return

    #...!...!....................
    def prep_train_block(self):
        if self.verb>0: print('train_block start')
        C=self.myConf['train']
        if self.verb>1: pprint(C)

        # decide if BN layer is added
        self.proposal['batch_norm_cnn']=np.random.uniform()< self.myConf['cnn_BN_prob']
        # add 2nd BN layer only of 1st BN-layer is chose and CNN_block_B is non-empty
        doBN2=False
        if self.proposal['batch_norm_cnn'] and len(self.proposal['cnn_block_B']['filters'])>0:
            doBN2=np.random.uniform()< self.myConf['flat_BN_prob']
        self.proposal['batch_norm_flat']=doBN2

        optName=str(np.random.choice(C['optimizer']))
        outD={'optName':optName,'lossName':'mse'}
        initLR=float(np.random.choice(C['initLR']))
        min_delLR=float(np.random.choice(C['min_deltaLR']))
        reduceLR=float(np.random.choice(C['reduceLR']))
        outD['LRconf']={'init':initLR, 'reduceFactor':reduceLR, 'patience':20,'min_delta':min_delLR}
        outD['multiAgent']={'warmup_epochs': 10}
        self.proposal['train_conf']=outD

    #...!...!....................
    def eval_train_speed(self,batch_size,steps,epochs):
        trainGen = simple_data_generator(X,Y, batch_size)

        fitVerb=1   # prints live:  [=========>....] - ETA: xx s 
        if self.verb==2: fitVerb=1
        if self.verb==0: fitVerb=2  # prints 1-line summary at epoch end

        gen.model.fit(trainGen,steps_per_epoch=steps, epochs=1,
                      verbose=fitVerb,
                      max_queue_size=10, workers=1, use_multiprocessing=False)

        if self.verb: print('restart train for %d epochs'%epochs)
        startT = time.time()
        gen.model.fit(trainGen,steps_per_epoch=steps, epochs=epochs,
                      verbose=fitVerb,
                      max_queue_size=10, workers=1, use_multiprocessing=False)

        totSec= time.time()-startT
        epochT=totSec/epochs
        samp_per_sec=batch_size*steps/epochT
        if self.verb>0: print('eval_train_speed  %.1f sec/epoch'%epochT)
        self.qa.update({'maxLocalBS':batch_size,'steps':steps,'samples_per_sec':samp_per_sec,'sec_per_epoch':epochT,'used_time':(time.time()- self.timeBegin)})
        return samp_per_sec


#...!...!....................



#...!...!....................
def create_fake_dataset(inpShape,outShape,numSamp):
    print('create_dataset: X,Y:',inpShape,outShape,'N:',numSamp)
    # The input data and labels
    startT = time.time()

    Y=np.random.uniform(-1,1, size=(numSamp,outShape)).astype('float32')

    inpL=inpShape.copy()
    inpL.insert(0,numSamp)
    X=np.random.uniform(-0.8,0.8, size=tuple(inpL)).astype('float32')
    print(' done X shape',X.shape,X.dtype,' elaT=%.1f sec,'%(time.time() - startT))
 
    return X,Y

#...!...!..................
def simple_data_generator(X,Y, bs):
    mxJ=Y.shape[0]
    assert mxJ>=bs
    print('dataset: X,Y:',X.shape,Y.shape)
    j=0
    # loop indefinitely
    while True:
        if j>=mxJ-1-bs: j=0 # reset pointer if not enough for next batch
        xb=X[j:j+bs]
        yb=Y[j:j+bs]
        j+=bs
        yield (xb,yb)
    
 
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.verb>0:
    Lkr=K._get_available_gpus()
    print('Seen GPU devCnt=%d '%len(Lkr),Lkr)

confN=args.genConf
conf=read_yaml(confN)
pprint(conf)

# generate minimalistic data set 
numSample=max(conf['train']['localBS'])
X,Y=create_fake_dataset(conf['inpShape'],conf['outShape'],numSample)

nok=0
for shot in range(args.numTrials):
    print('\n = = = = = = = = = = M:new shot',shot)
    gen=HparGenerator_CosmoFlow(conf,verb=(shot==0))
    gen.qa['facility']=args.facility

    propF=args.outPath+'/hpar_cosmo7_%s.yaml'%gen.proposal['myId']
    if not gen.prep_CNN_block('A'): continue
    if not gen.prep_CNN_block('B'): continue
    gen.prep_FC_block()
    gen.prep_train_block()

    # use utility function for eaiser porting of the model to DeepCosmoFlow
    try:
        gen.model,totParCnt=build_model_cosmo7(gen.proposal,verb=gen.verb)
    except:
        continue
    gen.qa['param_count']=totParCnt
    if args.verb:
        print('M:proposal:');  pprint(gen.proposal)

    if totParCnt >conf['constraints']['maxModelParameters'] or\
               totParCnt <conf['constraints']['minModelParameters']  :
        if args.verb>1: print('M: model is too large/small=%.3e param, ABANDON'%totParCnt,'shot=',shot)
        write_yaml(gen.proposal,propF+'-tmp',verb=gen.verb)
        continue

    # find minimal local BS
    localBS=None
    redBS=0
    for bs in conf['train']['localBS']:
        try:
            sampSpeed=gen.eval_train_speed(bs,args.steps,args.epochs)
            localBS=bs
            break
        except:
            if args.verb>1: print('\ntoo large BS %d, try next'%bs,shot)
            redBS=1
            continue

    if localBS==None or localBS <2: 
        if args.verb>1: print('M: local BS is',bs,'ABANDON','shot=',shot)
        continue

    if sampSpeed <conf['constraints']['minSamplesPerSec']:
        if args.verb>1: print('M: model is too large=%.3e param, ABANDON'%totParCnt,'shot=',shot)
        write_yaml(gen.proposal,propF+'-tmp',verb=gen.verb)
        continue

    write_yaml(gen.proposal,propF)

    if redBS : localBS-=1 # just in case
    fd=open(args.outPath+'/good_proposal_%s.txt'%gen.proposal['myId'],'w')
    fd.write('HparGood design: %s  localBS: %d   param_count: %.2e   samples_per_sec: %.1f  shot: %d\n'%(gen.proposal['myId'], localBS, gen.qa['param_count'],gen.qa['samples_per_sec'],shot))
    fd.close()

    nok+=1
    time.sleep(3)

print('M: done all %d trials, nOK=%d'%(args.numTrials,nok))
