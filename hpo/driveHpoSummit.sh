#!/bin/bash

echo D:driveHpoSummit 

# Disable multiple threads
export OMPI_MCA_osc_pami_allow_thread_multiple=0

#disable adaptive routing
export PAMI_IBV_ENABLE_OOO_AR=0
export PAMI_IBV_QP_SERVICE_LEVEL=0

# enable priority NCCL stream
grank=$PMIX_RANK
lrank=$(($PMIX_RANK%6))
export CUDA_VISIBLE_DEVICES=$lrank
#APP=" nvidia-smi  "

APP=" python -u ./genHPar_Cosmo.py --numTrials 30 --verb 0  "

echo "Full APP=$APP="
echo "shPWD="`pwd`

#startSkew=20 # (seconds), random delay for task
#nsleep=$(($RANDOM % $startSkew))
#echo nsleep=$nsleep
#sleep $nsleep

export PAMI_ENABLE_STRIPING=0

# . . . . .  FIRE THE TASK . . . . . . . .

case ${lrank} in
[0])
export PAMI_IBV_DEVICE_NAME=mlx5_0:1
numactl --physcpubind=0-27 --membind=0 $APP
  ;;
[1])
export PAMI_IBV_DEVICE_NAME=mlx5_1:1
numactl --physcpubind=28-55 --membind=0 $APP
  ;;
[2])
export PAMI_IBV_DEVICE_NAME=mlx5_0:1
numactl --physcpubind=56-83 --membind=0 $APP
  ;;
[3])
export PAMI_IBV_DEVICE_NAME=mlx5_3:1
numactl --physcpubind=88-115 --membind=8 $APP
  ;;
[4])
export PAMI_IBV_DEVICE_NAME=mlx5_2:1
numactl --physcpubind=116-143 --membind=8 $APP
  ;;
[5])
export PAMI_IBV_DEVICE_NAME=mlx5_3:1
numactl --physcpubind=144-171 --membind=8 $APP
  ;;
esac
