#!/usr/bin/env python
""" 

evaluate model designs generated by hper-param scan
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import  time

from collections import OrderedDict 

import sys,os
sys.path.append(os.path.abspath("toolbox")) # add ln -s for interactive testing
from Util_IOfunc import  write_yaml, read_yaml

from Plotter_Backbone import Plotter_Backbone
from matplotlib import cm as cmap

import argparse

def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    parser.add_argument("-j", "--jobId",nargs="+",default=['403282'],
                        help=" blank separated list of job IDs, takes n1-n2")

    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
    parser.add_argument("-m", "--maxLossMSEx1000", type=float, default=50,help="loss cut-off for plotting")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',  action='store_true', default=False, help="disable X-term for batch mode")


    args = parser.parse_args()
    args.prjName='cosmoHpo'

    #Cori
    #args.sourcePath='/global/cscratch1/sd/balewski/cosmo5c/'
    #args.sourcePath='/global/homes/b/balewski/0x/packSetD/'
    args.sourcePath='/global/homes/b/balewski/prjs/cosmoTest/cosmo7/'
    #args.sourcePath='../summit_hpo_setE/'
    # Summit:
    #args.sourcePath='/gpfs/alpine/world-shared/ast153/balewski/tmp4/'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_Design(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)

    #...!...!....................
    def overview(self,ordD,maxLoss,figId=5,title=''):
        lossV=[]; fitTimeV=[]; earlyV=[]; sizeV=[]; epochTimeV=[]
        
        for task,rec in ordD:  #extract data
            #print(task,rec)
            #print(list(rec))
            lossV.append(rec['testLossMSE*1000'])
            fitTimeV.append(rec['fitMin'])
            earlyV.append(rec['earlyStopOccured'])
            sizeV.append(np.log10(rec['modelWeightCnt']))
            epochTimeV.append(rec['glob_epoch_duration_sec'][0])
        pltObjL=[ ('test MSE-loss*1000',lossV),('fit time (min)',fitTimeV),('earlyStop',earlyV),('sec/epoch',epochTimeV),('log10(modelSize)',sizeV) ]

        nrow,ncol=1,5
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(13,2.5))

        bins=20

        for j,[name,data] in enumerate(pltObjL):
            vals=np.array(data)
            resM=vals.mean()
            resS=vals.std()
            ax=self.plt.subplot(nrow,ncol,j+1)
            ax.hist(vals, bins, alpha=0.5)
            
            ax.set(xlabel=name)
            avrTxt='avr: %.3g+/-%.2g'%(resM,resS)
            print('plot ',name,avrTxt)
            ax.set(title=avrTxt,xlabel=name)
            ax.axvline(resM, color='red', linestyle='--', linewidth=1.)
            ax.axvline(resM-resS, color='green', linestyle=':', linewidth=1.)
            ax.axvline(resM+resS, color='green', linestyle=':', linewidth=1.)
 
            if j==0: ax.text(0.1,0.85,'n=%d'%len(lossV),transform=ax.transAxes)
            if j==1: 
                ax.text(0.1,0.85,'%s'%title,transform=ax.transAxes)
                ax.text(0.1,0.75,'max MSE*100 thr=%.1f'%maxLoss,transform=ax.transAxes)
                
            ax.grid(True)
 
    #...!...!....................
    def hpar_Train(self,ordD,auxD):
        lossV=[]
        optKey=['adadelta', 'adagrad', 'adam', 'nadam']
        optV=[];  initLRV=[]; redLRV=[]
        midV=[]
        
        for task,rec in ordD: #extract data
            lossV.append(rec['testLossMSE*1000'])
            hpar=rec['hyperParams']
            trncf=hpar['train_conf']
            lrcf=trncf['LRconf']
            #print(trncf)
            optV.append(optKey.index(trncf['optName']))
            initLRV.append(np.log10(lrcf['init']))
            redLRV.append(np.log10(lrcf['reduceFactor']))
            midV.append(np.log10(lrcf['min_delta']))
            
        pltObjL=[ ['optimizer',optKey,optV], ['log10(initLR)',None,initLRV],
                  ['log10(LR reduce fact)',None,redLRV],
                  ['log10(LR min_delta)',None,midV]
        ]

        auxD={'tit1':'train_block, job=%s'%(auxD['jobId'])}
        self.hpar_generic(lossV,pltObjL,auxD)

    #...!...!....................
    def hpar_FC(self,ordD,auxD):
        lossV=[]
        actKey=['relu','selu']
        actV=[];  lyCntV=[]; dropV=[]; ladimV=[]
        bn_flatV=[]; bn_cnnV=[]
        for task,rec in ordD: #extract data
            lossV.append(rec['testLossMSE*1000'])
            hpar=rec['hyperParams']
            fccf=hpar['fc_block']
            actV.append(actKey.index(fccf['activation']))
            lyCntV.append(len(fccf['units']))
            ladimV.append(np.log2(fccf['units'][-1]))
            dropV.append(fccf['dropFrac'])
            bn_cnnV.append(hpar['batch_norm_cnn'])
            bn_flatV.append(hpar['batch_norm_flat'])
            
        pltObjL=[ ['FC act', actKey,actV], ['num layers',None, lyCntV],
                  ['dropFrac',None,dropV], ['log2(last dim)',None,ladimV],
                  ['BN-cnn',None,bn_cnnV], ['BN-flat',None,bn_flatV]]
        auxD={'tit1':'FC_block, job=%s'%(auxD['jobId'])}
        self.hpar_generic(lossV,pltObjL,auxD)

    #...!...!....................
    def hpar_CNN(self,ordD,block,auxD):
        lossV=[]
        actKey=['relu','selu']; actV=[]
        ptKey=['avr','max']; ptV=[]
        kerV=[]; poolsV=[]; strV=[]
        lyCntV=[]
        
        for task,rec in ordD: #extract data
            lossV.append(rec['testLossMSE*1000'])
            hpar=rec['hyperParams']
            cnncf=hpar['cnn_block_'+block]
            actV.append(actKey.index(cnncf['activation']))
            ptV.append(ptKey.index(cnncf['pool_type']))
            kerV.append(cnncf['kernel'])
            strV.append(cnncf['strides'])
            poolsV.append(cnncf['pool_size'])
            lyCntV.append(len(cnncf['filters']))
        pltObjL=[  ['kernel',None,kerV], ['strides',None, strV],
                   ['pool_size',None,poolsV] , ['num layers',None, lyCntV],
                   ['act_'+block, actKey,actV],['pool_type',ptKey,ptV]]
        auxD={'tit1':'CNN_block_%s, job=%s'%(block,auxD['jobId'])}
        self.hpar_generic(lossV,pltObjL,auxD)
        
    #...!...!....................
    def hpar_generic(self,lossV,pltObjL,auxD,figId=7,title=''):
        colMap=cmap.rainbow
        nrow,ncol=2,3
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(8.5,4.5))

        for j,[name,xlab,data] in enumerate(pltObjL):
            valV=np.array(data)
            ax=self.plt.subplot(nrow,ncol,j+1)
            binX=10
            if xlab!=None:
                nxlab=len(xlab)
                binX=np.linspace(-0.5,nxlab-0.5,nxlab+1)
            #print('xb',binX)
            zsum,xbins,ybins,img = ax.hist2d(valV,lossV,bins=(binX,8), cmin=1,cmap = colMap)

            self.plt.colorbar(img, ax=ax)
            ax.set(xlabel=name,ylabel='MSE loss * 1000')

            if j==0: ax.set(title=auxD['tit1'])
            if  xlab!=None:
                xticks=[i for i in range(0,nxlab)]
                ax.set_xticks(xticks)
                ax.set_xticklabels(xlab, rotation=45)

# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
#...!...!....................
def locateSummaryData(src_dir,jobId,stockD,verb=1):

    for xx in [ src_dir+jobId]:
        if os.path.exists(xx): continue
        print('Aborting on start, missing  dir:',xx)
        exit(1)

    if verb>0: print('use src_dir:',src_dir,'jobId:',jobId)

    jobL=os.listdir(src_dir+jobId)
    print('locateSummaryData got %d potential jobs, e.g.:'%len(jobL), jobL[0])
    print('sub-dirs',jobL)
        
    veryBadS=set()
    # at this point the list of dirs is fixed, now picking of sum.yaml

    cnt={'inp':0,'train':0,'noTrain':0,'pred':0,'noPred':0}
    for taskId in jobL:
        cnt['inp']+=1
        dataPath=src_dir+jobId+'/'+taskId+'/out/'
        sumF=None
        anyL=os.listdir(dataPath)
        for x in anyL:
            if 'sum_train.yaml' not in x: continue 
            sumF=dataPath+'/'+x 
            break
        if sumF==None:
            cnt['noTrain']+=1
            print(' missing train-summary for job/taskId=',jobId,taskId,' skip',sumF)
            continue
        cnt['train']+=1

        sumF2=sumF.replace('_train','_pred')
        if verb>1: print('open sumF=',sumF)

        if 0: # manuall data copy to Cori
            goalHead='/gpfs/alpine/world-shared/ast153/balewski/tmp4/packSetE/'
            target=goalHead+jobId+'/'+taskId+'/out/'
            print('\n##keep\n mkdir -p %s \ncp %s %s\ncp %s %s\n'%(target,sumF,target,sumF2,target))
        
        try:
            sumD=read_yaml(sumF)
            #assert sumD['state']=='model_trained'
            if sumD['state']!='model_trained':
                fix_me16

            sumD2=read_yaml(sumF2)
            sumD['testLossMSE*1000']=1000.*sumD2['testLossMSE']
            
        except:
            #crashIt34
            if not os.path.exists(sumF2):
                cnt['noPred']+=1
                print('fix-pred',dataPath)
                print('Repred %s %s/%s'%(sumD['modelDesign'] ,jobId,taskId))
                #bad45
            continue

        # update total training time by adding pilot1 & polit2
        for pp in ['1','2']:
            #break # for summit_hpo_setE/410081
            sumFx=sumF2.replace('_pred','_pilot'+pp)
            sumDx=read_yaml(sumFx,verb=0)
            sumD['fitMin']+=sumDx['fitMin']
            
        cnt['pred']+=1
 
        if verb>1 or len(stockD)==0:print(sumD)
        sumD['dataPath']=src_dir+jobId+'/'+taskId+'/out/'
        key=sumD['modelDesign']
        dup=0
        while key in stockD:
            dup+=1
            key='%s_%d'%(sumD['modelDesign'],dup)
        stockD[key]=sumD
    print('end of locate jobId=',jobId,' cnt:',cnt,len(stockD))
    if cnt['pred'] <=0:
        print('no predictions found, quit')
        exit(0)

 #=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

stockD={}
for jobId in args.jobId:
    locateSummaryData(args.sourcePath, jobId, stockD,verb=args.verb)
print('M: got stock, size=',len(stockD))
#print('M: g stock:',sorted(stockD))
assert len(stockD)>0


ordD=OrderedDict(sorted(stockD.items(), key = lambda x: x[1]['testLossMSE*1000'], reverse = False)) 
print('\nranked designs\nrank, design, lossMSE*1000, path') 
nGood=0
nSeen=len(ordD)
for x in ordD:
    loss=ordD[x]['testLossMSE*1000']
    path=ordD[x]['dataPath']
    if loss>args.maxLossMSEx1000: break
    #if loss<args.maxLossMSE: continue
    nGood+=1
    print('%d %s %.2f %s'%(nGood,x,loss,path))


# clip results above loss threshold 
ordD=list(ordD.items())[:nGood]
print('M:nGood=%d of %d'%(nGood,nSeen))
#print(ordD)
plot=Plotter_Design(args)

auxD={'jobId':jobId,'maxLoss':args.maxLossMSEx1000}
plot.overview(ordD, title='jid='+jobId,maxLoss=args.maxLossMSEx1000)

if 0:  # hpar correlations
    plot.hpar_Train(ordD,auxD)    
    plot.hpar_FC(ordD,auxD)
    plot.hpar_CNN(ordD,'A',auxD)
    plot.hpar_CNN(ordD,'B',auxD)

plot.display_all('rank')  
