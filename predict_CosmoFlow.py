#!/usr/bin/env python
""" 
read input hd5 tensors
read trained net : model+weights
read test data from HD5
evaluate test data 

using last model:
./predict_CosmoFlow.py --dataSplit cosmo7_dataSplit_small.yaml -X  -n 70 

it may need   export CUDA_VISIBLE_DEVICES=2 

using best model:

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


import numpy as np
import  time
from tensorflow.python.keras import backend as K  # just for GPU count
import sys,os
sys.path.append(os.path.abspath("toolbox"))
from Plotter_CosmoFlow import Plotter_CosmoFlow
from Deep_CosmoFlow import Deep_CosmoFlow
from Util_Func import  get_UmZ_correl
from Util_IOfunc import  read_yaml,  write_yaml
from InpGen_CosmoFlow import Cosmo7_input_generator

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')

    parser.add_argument('--design', dest='modelDesign', default='lbann2',help=" model design of the network")

    parser.add_argument("--dataPath",help="path to input",default='data')

    parser.add_argument("--seedModel",default='out/', help="'last' trained model and weights")

    parser.add_argument("--seedWeights", default=None, help="seed 'best' weights only, after model is created")

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
    parser.add_argument("--dataSource",type=str,  default='csc',help="path to data vault: cscratch or BB")
    parser.add_argument("--dataSplit",default='cosmo7_dataSplit_128cube12chan.yaml',help="how to split data onto train/val/test")

    parser.add_argument("--dom",default='test', help="domain is the dataset for which predictions are made, typically: test",choices=['train','test','val'])

    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False, help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=100, help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='cosmoFlow'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
Lkr=K._get_available_gpus()
print('Seen GPU(s) devCnt=%d '%len(Lkr),Lkr)
args=get_parser()
deep=Deep_CosmoFlow.predictor(args)
plot=Plotter_CosmoFlow(args,deep.metaD  )

dom=args.dom

# deep.print_frames('test',numFrm=1)
if args.seedModel=='same' :  args.seedModel=args.outPath
deep.load_model_full(args.seedModel)

if args.seedWeights:
    if args.seedWeights=='same' : args.seedWeights=args.seedModel
    deep.load_weights_only(path=args.seedWeights)

# instantiate input generator
inpF=args.seedModel+'/'+deep.prjName+'.sum_train.yaml'
blob=read_yaml(inpF)
#print('M:bbb', blob.keys())
genConf=blob['inpGen']['train']
genConf.pop('fileIdxList') # just in case to not re-use the same data in predict.
#print('M:www',genConf)

genConf['numLocalSamples']=args.events
genConf['fileIdxList']= deep.metaD['splitIdx'][dom]
genConf['name']='IG-'+dom
genConf['domain']=dom
genConf['shuffle']=False # for reproducibility
genConf['x_y_aux']=True # it will produce 3 np-arrays per call

#.... creates data generator
inpGen=Cosmo7_input_generator(genConf,verb=1)

steps=max(1,args.events//genConf['localBS'])
mxSteps=inpGen.__len__()
steps=min(mxSteps,steps)

print('start predicting for dom=',dom,' num data=',args.events,' steps=',steps, ' mxSteps=',mxSteps,'BS=',genConf['localBS'])
assert mxSteps >= steps # prevents re-use test data (beyond 1  true epoch)

startT0 = time.time()

uL=[]; zL=[]
for i in range(steps):
    x,u,aux =inpGen.__getitem__(i)
    z= deep.model.predict(x)
    if i%10==0 :  print('M:%d step of %d done'%(i,steps))
    uL.append(u)
    zL.append(z)
    
predTime=time.time() - startT0
print('pred-done, eve=%d, elaT=%.1f sec,'%(args.events,predTime))

# add all steps 
U=np.concatenate(tuple(uL),axis=0)
Z=np.concatenate(tuple(zL),axis=0)

print('got predicted Z, shape:',Z.shape)
print('Z sample:',Z[:2])
UmZ=U-Z

metaD={'redshifIdx':[ 'redshift_%d'%x for x in range(x.shape[2])]}
#1plot.frames_vsTime(x,u,2,metaD=metaD) #hack

tit='dom=%s'%(dom)
#1plot.fparams(z,'pred Z',figId=8,tit2=tit)

lossMSE=get_UmZ_correl(UmZ,dom,deep.metaD['parName'])
metaD=deep.metaD

#1plot.fparams(Z,'pred Z',figId=8)
#1plot.fparams(U,'true U',figId=7)

sumRec={}
sumRec[dom+'LossMSE']=float(lossMSE)
sumRec['predTimeMnt']=predTime/60.
sumRec['numSamples']=u.shape[0]
sumRec['design']=deep.modelDesign

write_yaml(sumRec, deep.outPath+'/'+deep.prjName+'.sum_pred.yaml')
tit='dom=%s '%(dom)
plot.physParam_residu(U,Z, figId=9,tit=tit, tit2='lossMSE=%.3g'%lossMSE,tit3='design='+deep.modelDesign)

plot.display_all('predict')  







