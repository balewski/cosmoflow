#!/usr/bin/env python3
""" 
testing performance of input generator (Keras)
read test data from HD5

Use cases, needs valid config: 3inhib_3prB8kHz.yaml 

./testInpGen_CosmoFlow.py  (lot's of data, takes ~1 minute)

OR minimal
./testInpGen_CosmoFlow.py  --dataSplit cosmo7_dataSplit_small.yaml -n 10

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import sys,os
sys.path.append(os.path.abspath("toolbox"))

import numpy as np
import  time
import ruamel.yaml  as yaml
from pprint import pprint

from InpGen_CosmoFlow import Cosmo7_input_generator
from Deep_CosmoFlow import Deep_CosmoFlow

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataSource",type=str,
                        default='cfs',help="path to data vault: cscratch or BB")

    parser.add_argument("--dataSplit",
                        default='cosmo7_dataSplit_128cube12chan.yaml',
                        help="defines how to split data onto train/val/test")

    parser.add_argument("--dom",default='test', help="domain is the dataset for which predictions are made, typically: test",choices=['train','test','val'])

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False, help="disable X-term for batch mode")

    parser.add_argument("-n", "--events", type=int, default=1024, help="events for training, use 0 for all")

    args = parser.parse_args()
    args.prjName='testIG'
    args.outPath+='/'
    args.modelDesign='fake2'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
deep=Deep_CosmoFlow.predictor(args)
metaD=deep.metaD
dom=args.dom
genConf={}
genConf['name']='IG-'+dom
genConf['domain']=dom
genConf['myRank']=3
genConf['localBS']=2
genConf['x_y_aux']=True # it will produce 3 np-arrays per call
genConf['numLocalSamples']=args.events # genConf['localBS']*32
genConf['dataPath']=deep.dataPath

genConf['h5nameTemplate']=metaD['h5nameTemplate']
genConf['fileIdxList']=metaD['splitIdx'][dom]
genConf['sampleShape']=metaD['sampleShape']
genConf['sampleNorm']=metaD['sampleNorm']
genConf['numPar']=metaD['numPar']
genConf['shuffle']=True


pprint(genConf)
#.... creates data generator
inpGen=Cosmo7_input_generator(genConf,verb=2)

steps=max(1,args.events//genConf['localBS'])
mxSteps=inpGen.__len__()
if 1: # restrict to 1 epoch, no data repetition
    steps=min(mxSteps,steps)

print('\nM:start processing dom=',dom,' num samples=',args.events,' steps=',steps, ' mxSteps/epoch=',mxSteps,'BS=',genConf['localBS'])

time.sleep(5) # too see memory usage, tmp
startT0 = time.time()
uL=[]
for i in range(steps):
    x,u,aux =inpGen.__getitem__(i)
    #if i%100==0 :  print('M:%d step of %d done, shapes u,z:'%(i,steps),u.shape)
    uL.append(u)

predTime=time.time() - startT0
print('RAM-read done, eve/k=%d, elaT=%.3f sec,'%(args.events/1000.,predTime))
print('batch shape X:',x.shape,'U:',u.shape)

#print('IG stepTimeHist:'); pprint(inpGen.stepTime)

# add all steps 
U=np.concatenate(tuple(uL),axis=0)

avrU=np.mean(U,axis=0)
stdU=np.std(U,axis=0)
parNL=deep.metaD['parName']
print('got U avr,std:',U.shape)
for i in range(avrU.shape[0]):
    print("idx=%d  avr=%.3f  std=%.3f  %s"%(i, avrU[i],stdU[i],parNL[i]))









