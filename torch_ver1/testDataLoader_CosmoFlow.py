#!/usr/bin/env python3
""" 
testing performance of input generator (PyTorch)
read test data from HD5

Use cases, needs valid config: 3inhib_3prB8kHz.yaml 

./testDataLoader_CosmoFlow.py  --localSamples 256 --dom train (lot's of data, takes ~2 minutes for train)

OR minimal
./testDataLoader_CosmoFlow.py  --dataSplit cosmo8_dataSplit_small.yaml --localSamples 10

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import sys,os
sys.path.append(os.path.abspath("toolbox"))

import numpy as np
import  time
import ruamel.yaml  as yaml
from pprint import pprint

from Deep_CosmoFlow import Deep_CosmoFlow
from Dataset_CosmoFlow  import Dataset_CosmoFlow
from torch.utils.data import  DataLoader

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    parser.add_argument("--dataSource",type=str,
                        default='cfs',help="path to data vault: cscratch or BB")

    parser.add_argument("--dataSplit",
                        default='cosmo8_dataSplit_128cube12chan.yaml',
                        help="defines how to split data onto train/val/test")

    parser.add_argument("--dom",default='test', help="domain is the dataset for which predictions are made, typically: test",choices=['train','test','val'])

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False, help="disable X-term for batch mode")

    parser.add_argument("-n", "--localSamples", type=int, default=5, help="samples to read")


    args = parser.parse_args()
    args.prjName='testIG'
    args.outPath+='/'
    args.modelDesign='a1_e5bb4b43'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
deep=Deep_CosmoFlow.predictor(args)
metaD=deep.metaD
dom=args.dom
dstConf={}
for x in [ 'sampleNorm','h5nameTemplate']: #,
    dstConf[x]=deep.metaD['dataInfo'][x]
for x in ['inputShape','outputSize']:
    dstConf[x]=deep.hparams[x]
dstConf['numLocalSamples']=args.localSamples
dstConf['dataPath']=deep.dataPath
dstConf['shuffle']=True  # use False for reproducibility
dstConf['x_y_aux']=True # it will produce 3 np-arrays per call
dstConf['name']='DST-'+dom
dstConf['domain']=dom
dstConf['myRank']=12
dstConf['dataList']=deep.metaD['dataList'][dom]

print('\nM:Create torch-Dataset instance')
pprint(dstConf)
inpDst=Dataset_CosmoFlow(dstConf,verb=2)
localBS=8
numLocalCPU=2  # -c10 locks 5 physical cpu cores
inpDst.sanity(localBS)


print('\nCreate torch-DataLoader instance & test it')
trainLdr = DataLoader(inpDst, batch_size=localBS, shuffle=True, num_workers=numLocalCPU, pin_memory=True)
xx, yy,aux = next(iter(trainLdr))
print('test_dataLoader 1 batch: X:',xx.shape,'Y:',yy.shape)
print('Y[:,]',yy[:,0])

steps=len(trainLdr)

print('\nM:start processing dom=',dom,' localSamples=',args.localSamples,' steps=',steps, 'localBS=',localBS)

time.sleep(5) # too see memory usage, tmp
startT0 = time.time()
uL=[]
for i in range(steps+1):
    x,u,aux =next(iter(trainLdr))
    #if i%100==0 :  print('M:%d step of %d done, shapes u,z:'%(i,steps),u.shape)
    uL.append(u)

predTime=time.time() - startT0
print('RAM-read done, samples/k=%d, elaT=%.3f sec,'%(args.localSamples/1000.,predTime))
print('batch shape X:',x.shape,'U:',u.shape)

# add all steps 
U=np.concatenate(tuple(uL),axis=0)

avrU=np.mean(U,axis=0)
stdU=np.std(U,axis=0)
parNL=deep.metaD['dataInfo']['outputName']
print('got U avr,std:',U.shape)
for i in range(avrU.shape[0]):
    print("idx=%d  avr=%.3f  std=%.3f  %s"%(i, avrU[i],stdU[i],parNL[i]))

