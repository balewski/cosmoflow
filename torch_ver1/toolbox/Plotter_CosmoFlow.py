__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np

from matplotlib import cm as cmap
from toolbox.Util_CosmoFlow import get_rho_correl

from toolbox.Plotter_Backbone import Plotter_Backbone

#............................
#............................
#............................
class Plotter_CosmoFlow(Plotter_Backbone):
    def __init__(self, args,metaD):
        Plotter_Backbone.__init__(self,args)

        self.maxU=1.4
        self.metaD=metaD

         
#...!...!....................#............................
    def fparam_residu(self,U,Z, figId=9,tit='',tit2=''):

        colMap=cmap.rainbow
        outputNameL=self.metaD['dataInfo']['outputName']
        nPar=self.metaD['dataInfo']['outputSize']

        nrow,ncol=3,nPar
        breakPar=0
        if nPar>15:
            breakPar=int(np.ceil(nPar/2.) )
            nrow,ncol=4,breakPar

        #  grid is (yN,xN) - y=0 is at the top,  so dum
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(ncol*3.2,nrow*2.6))
        assert U.shape[1]==Z.shape[1]

        j=0
        for iPar in range(0,nPar):
            j=iPar
            jOff=1
            if iPar >=breakPar:
                jOff=2*breakPar
                j=iPar-breakPar+1
            #print('dd',iPar,j,jOff)
            ax1 = self.plt.subplot(nrow,ncol, j+jOff); ax1.grid()
            ax2 = self.plt.subplot(nrow,ncol, j+ncol*1+jOff); ax2.grid()

            u=U[:,iPar]
            z=Z[:,iPar]
            unitStr='(a.u.)'
            mm2=self.maxU
            mm1=-mm2
            mm3=self.maxU/1.  # adjust here of you want narrow range for 1D residues
            binsX=np.linspace(mm1,mm2,30)
            rho,sigRho=get_rho_correl(u,z)
            
            zsum,xbins,ybins,img = ax1.hist2d(z,u,bins=binsX, cmin=1,
                                              cmap = colMap)
            # beutification
            self.plt.colorbar(img, ax=ax1)
            ax1.plot([0, 1], [0,1], color='magenta', linestyle='--',linewidth=1,transform=ax1.transAxes) #diagonal
            #

            ax1.set(title='%d:%s'%(iPar,outputNameL[iPar]), xlabel='pred %s'%unitStr, ylabel='true')
            ax1.text(0.2,0.1,'rho=%.3f +/- %.3f'%(rho,sigRho),transform=ax1.transAxes)

            x0=0.07
            if iPar==0: ax1.text(x0,0.82,tit,transform=ax1.transAxes)
            if iPar==1: ax1.text(x0,0.92,'n=%d'%(u.shape[0]),transform=ax1.transAxes)
            if iPar==0: ax1.text(x0,0.92,tit2,transform=ax1.transAxes)

            # .... compute residue
            zmu=z-u
            resM=zmu.mean()
            resS=zmu.std()

            # ..... 1D residue
            binsU= np.linspace(-mm3,mm3,50)
            ax2.hist(zmu,bins=binsU)

            ax2.set(title='res: %.2g+/-%.2g'%(resM,resS), xlabel='pred-true %s'%unitStr, ylabel='traces')
            ax2.text(0.15,0.8,outputNameL[iPar],transform=ax2.transAxes)

            if breakPar>0: continue
            # ......  2D residue
            ax3 = self.plt.subplot(nrow,ncol, j+ncol*2+jOff); ax3.grid()
            zsum,xbins,ybins,img = ax3.hist2d(z,zmu,bins=[binsX,binsX/2], cmin=1,cmap = colMap)
            self.plt.colorbar(img, ax=ax3)
            ax3.set( xlabel='pred %s'%unitStr, ylabel='pred-true %s'%unitStr)
            ax3.axhline(0, color='green', linestyle='--')
            ax3.set_xlim(mm1,mm2) ; ax3.set_ylim(mm1,mm2)


#...!...!....................
    def data_fparams(self,U,tit,figId=6):
        figId=self.smart_append(figId)

        outputNameL=self.metaD['dataInfo']['outputName']
        nPar=self.metaD['dataInfo']['outputSize']

        nrow,ncol=2,int(nPar/2+1.5) # displays 2 pars per plot
 
        fig=self.plt.figure(figId,facecolor='white', figsize=(3.2*ncol,2.5*nrow))

        mm=self.maxU
        binsX= np.linspace(-mm,mm,30)

        j=1
        for i in range(0,nPar,2):
            iPar=min(i,U.shape[1]-2) # account for odd num of params
            y1=U[:,iPar]
            y2=U[:,iPar+1]
            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            zsum,xbins,ybins,img = ax.hist2d(y1,y2,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            self.plt.colorbar(img, ax=ax)
            ax.set(title=tit, xlabel='%d: %s'%(iPar,outputNameL[iPar]), ylabel='%d: %s'%(iPar+1,outputNameL[iPar+1]))
            ax.grid()

        for i in range(0,nPar):
            y1=U[:,i]
            ax=self.plt.subplot(nrow,ncol,j) ; j+=1
            ax.hist(y1,bins=50)
            ax.set(xlabel='%d: %s'%(i,outputNameL[i]))
            ax.set_xlim(-mm,mm)
            ax.grid()
