__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.optim  import lr_scheduler

import numpy as np
import os, sys


#-------------------
#-------------------
#-------------------
class CNNandFC_Model(nn.Module):
    ''' Automatically   compute the size of the output of CNN+Pool portion of the model,  needed as input to the first FC layer 
    '''
#...!...!..................
    def __init__(self,hpar,verb=0):
        super(CNNandFC_Model, self).__init__()
        if verb: print('CNNandFC_Model hpar=',hpar)
        inp_chan=12
        self.inp_shape=tuple(hpar['inputShape']) # 2DCNN needs feature dimension speciffied
        self.verb=verb
        if verb: print('CNNandFC_Model inp_shape=',self.inp_shape,', verb=%d'%(self.verb))
        
        self.cnn_block = nn.ModuleList()
        self.fc_block  = nn.ModuleList()
        cnnD=hpar['cnn_block']
        fcD=hpar['fc_block']
        
        # .....  CNN layers
        for out_chan in cnnD['filter']:
            self.cnn_block.append( nn.Conv3d(inp_chan, out_chan, cnnD['kernel'],cnnD['strides'] ))
            self.cnn_block.append( nn.MaxPool3d(cnnD['pool_size']))
            self.cnn_block.append( nn.ReLU())
            inp_chan=out_chan
        self.cnn_block.append( nn.Flatten() )
            
        # compute FC_block input size
        with torch.no_grad():
            # process 2 fake examples through the CNN portion of model
            x1=torch.tensor(np.zeros((2,)+self.inp_shape), dtype=torch.float32)
            y1=self.forwardCnnOnly(x1)
            self.flat_dim=np.prod(y1.shape[1:]) 
            if verb>1: print('myNet flat_dim=',self.flat_dim)

        # .... add FC  layers
        inp_dim=self.flat_dim                
        if hpar['batch_norm_flat']: # With Learnable Parameters
            self.fc_block.append( nn.BatchNorm1d(inp_dim) )            
        for i,dim in enumerate(fcD['units']):
            self.fc_block.append( nn.Linear(inp_dim,dim))
            inp_dim=dim
            self.fc_block.append( nn.SELU())
            if fcD['dropFrac']>0 : self.fc_block.append( nn.Dropout(p= fcD['dropFrac']))

        #.... the last FC layer will have different activation and no Dropout
        self.fc_block.append(nn.Linear(inp_dim,hpar['outputSize']))

        # add last layer with proper activation
        outAmpl=1.2
        self.fc_block.append(nn.Tanh())
        self.fc_block.append(LambdaLayer(lambda x: x*outAmpl))

#...!...!..................
    def forwardCnnOnly(self, x):
        if self.verb>1: print('J0: inp2cnn',x.shape)
        # flatten 2D image 
        #x=x.view((-1,)+self.inp_shape )
        if self.verb>1: print('J: inp2cnn',x.shape)
        for i,lyr in enumerate(self.cnn_block):
            x=lyr(x)
            if self.verb>1: print('Jcnn: ',i,x.shape)
        return x
        
#...!...!..................
    def forward(self, x):
        if self.verb>1: print('JF: inF',x.shape,'numLayers CNN=',len(self.cnn_block),'FC=',len(self.fc_block))
        x=self.forwardCnnOnly(x)
        #?x = x.view(-1,self.flat_dim)
        if self.verb>1: print('JF1:',x.shape)
        for i,lyr in enumerate(self.fc_block):
            x=lyr(x)
            if self.verb>1: print('Jfc: ',i,x.shape)
        if self.verb>1: print('JF: y',x.shape)
        return x

#...!...!..................
    def summary(self):
        numLayer=sum(1 for p in self.parameters())
        numParams=sum(p.numel() for p in self.parameters())
        return {'modelWeightCnt':numParams,'trainedLayerCnt':numLayer,'modelClass':self.__class__.__name__}


#-------------------
#-------------------
#-------------------
class LambdaLayer(nn.Module):
    def __init__(self, lambd):
        super(LambdaLayer, self).__init__()
        self.lambd = lambd
    def forward(self, x):
        return self.lambd(x)



#-------------------
#-------------------
#-------------------
class EarlyStopping:
    """
    Early stops the training if validation loss doesn't improve after a given patience.
    Author: Bjarte Mehus Sunde
    source: https://github.com/Bjarten/early-stopping-pytorch/blob/master/pytorchtools.py
    modiffied by Jan Balewski
    """
    def __init__(self, patience=7, verbose=False, delta=0, warmup=5, chkptFile=None):
        """
        Args:
            patience (int): How long to wait after last time validation loss improved.
            verbose (bool): If True, prints a message for each validation loss improvement. 
            delta (float): Minimum change in the monitored quantity to qualify as an improvement.
            warmup (int): initial sleep period to avoid frequent chkpt
            chktpFile (string): full chkpt file name
        """
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.val_loss_min = np.Inf
        self.delta = delta
        self.warmup=warmup
        self.chkptFile=chkptFile
        self.epochCnt=0
        if self.verbose: print('EarlyStopping cstr, chkptFile=',chkptFile,', patience=%d, delta=%.3g'%(patience,delta))

    def __call__(self, val_loss, model):
        score = -val_loss
        self.epochCnt+=1
        if self.best_score is None:  # jan- drop it later
            self.best_score = score
            self.save_checkpoint(val_loss, model)
        elif score < self.best_score - self.delta:
            self.counter += 1
            #print('  EarlyStopping: patience=%d,  worse loss, old:  %.3g  --> new %.3g '%(self.counter,self.val_loss_min,val_loss))
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
            self.counter = 0
        return self.early_stop

    def save_checkpoint(self, val_loss, model):
        '''Saves model when validation loss decrease.'''
        if self.verbose:
            print('  EarlyStopping: patience=%d,  val-loss decreased:  %.4g  -->  %.4g '%(self.counter,self.val_loss_min,val_loss))
        if self.epochCnt > self.warmup and self.chkptFile!=None:
            if self.verbose: print('  EarlyStopping: saving state ...',self.chkptFile)
            torch.save(model.state_dict(), self.chkptFile)
        self.val_loss_min = val_loss



    
# = = = = = = = = = = = = = = = 
def model_train(deep):
    model=deep.model
    device=deep.device
    train_loader=deep.dataLoader['train']  # hardcoded
    optimizer=deep.optimizer
    model.train()
    train_loss=0
#    for batch_idx, (data, target) in enumerate(train_loader):
    for data, target, _ in train_loader:
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        lossOp = deep.loss_func(output, target)
        lossOp.backward()
        optimizer.step()
        train_loss +=lossOp.item()
    train_loss /= len(train_loader)
    return train_loss


# = = = = = = = = = = = = = = = 
def model_infer(deep,dom,outType=0):
    ''' outType = 
    0: training mode, output only loss
    1: prediction for only 1 batch, output all: loss,X,Y,U as numpy-array
    2: prediction for many batches, output all, concatenated using Thorsten's idea
    '''
    model=deep.model
    device=deep.device
    
    test_loader=deep.dataLoader[dom] 
    model.eval()
    test_loss = 0

    if outType==2 : # prepare output container, Thorsten's idea
        num_eve=len(test_loader.dataset)
        outputSize=deep.metaD['dataInfo']['outputSize']
        print('predict for num_eve=',num_eve,', outputSize=',outputSize)
        # clever list-->numpy conversion, Thorsten's idea
        Uall=np.zeros([num_eve,outputSize],dtype=np.float32) 
        Zall=np.zeros([num_eve,outputSize],dtype=np.float32) 
        nEve=0
        
    with torch.no_grad():
        for data, target, utrue in test_loader:
            data_dev, target_dev = data.to(device), target.to(device)
            output_dev = model(data_dev)
            lossOp=deep.loss_func(output_dev, target_dev)
            #print('qq',lossOp,len(test_loader.dataset),len(test_loader)); ok55
            test_loss += lossOp.item()
            output=output_dev.cpu()
            if outType==1:
                return test_loss,np.array(data),np.array(target), np.array(output)
            if outType==2:
                nEve2=nEve+target.shape[0]
                #print('nn',nEve,nEve2)
                Uall[nEve:nEve2,:]=target[:]
                Zall[nEve:nEve2,:]=output[:]
                nEve=nEve2
    test_loss /= len(test_loader)
    if outType==0:
        return test_loss
    if outType==2:
        return test_loss,Uall,Zall
        

# = = = = = = = = = = = = = = = 
def save_model_full(outF,model,optimizer,currEpoch):
    torch.save({'epoch': currEpoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
             }, outF)
    xx=os.path.getsize(outF)/1048576
    print('  closed PyTorch chkpt:',outF,' size=%.2f MB'%xx)

    
# = = = = = = = = = = = = = = = 
def load_model_full(inpF,model,optimizer):
    print(' load PyTorch chkpt:',inpF)
    checkpoint = torch.load(inpF)
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    lastEpoch = checkpoint['epoch']
    return lastEpoch


#...!...!..................
def try_barrier(rank,txt):
    """Attempt a barrier but ignore any exceptions"""
    print('at BAR, rank %d'%rank,txt)

    try:
        dist.barrier()
    except:
        pass

