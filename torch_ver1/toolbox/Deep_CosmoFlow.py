__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import os, sys, time
import socket # for hostname
import numpy as np
import h5py
import copy
import torch
from torch.utils.data import  DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau

from Util_Torch import  model_train, model_infer, save_model_full, try_barrier
from Util_Torch import CNNandFC_Model, EarlyStopping
from Util_IOfunc import write_yaml, read_yaml, myRank_Slurm
from Dataset_CosmoFlow  import Dataset_CosmoFlow

VERBOSE_RANKS=[0]  # select here which ranks will print, it is a list

'''  MISSING:????
- count epochs form1, propagate  epoch after restart
- EarlyStop : save state_best.pth after  after reaching first plataou, patince=5 is set in hpar.yaml
'''

#............................
#............................
#............................
class Deep_CosmoFlow(object):

#...!...!..................
    def __init__(self,**kwargs):
        for k, v in kwargs.items():            
            self.__setattr__(k, v)

        for xx in [  self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)

        self.headNodeName=socket.gethostname()
        self.myRank=0
        self.locRank=0
        self.numRanks=1  # will be changed for dist-GPU training
        self.sumRec={}
        self.hparF='./hpar_cosmo8_'+self.modelDesign+'.yaml'

 
# * * * *   alternative constructors   * * * * 
#...!...!..................
    @classmethod 
    def trainer(cls, args):
        #print('Cnst:train')
        obj=cls(**vars(args))
          
        if obj.device=='cuda':
            rank,world_size,loc_rank=myRank_Slurm()
            obj.myRank=rank
            obj.locRank=loc_rank
            obj.numRanks=world_size
            obj.verb*=rank in VERBOSE_RANKS  # make silent other ranks

            print('train:myRank=',rank,'world_size =',world_size,'locRank=',loc_rank,obj.headNodeName,'myVerb=',obj.verb )
            if obj.verb:
                for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))

            if world_size>1:    
                masterIP=os.getenv('MASTER_ADDR')
                masterPort=os.getenv('MASTER_PORT')
                if masterIP==None:
                    if loc_rank==0: print(obj.headNodeName+' undefined env: MASTER_ADDR and/or MASTER_PORT,  abort')
                    exit(99)
                if obj.verb: print('use masterIP:port',masterIP,masterPort)
                import torch.distributed as dist
                dist.init_process_group(backend='nccl', init_method='env://', world_size=world_size, rank=rank)
                obj.torch_dist=dist
                try_barrier(rank,'init')
        else:
            print('CPU-based one-node training')
           
        if obj.verb: print('use PyTorch ver:',torch.__version__)

        # . . . .   date description
        obj.read_metaInp(obj.dataSplit)

        obj.sumRec={}
        
        # . . . load hyperparameters
        obj.load_hyperParams()

        # overwrite some hpar if provided from command line
        if obj.localBS!=None:
            obj.hparams['train_conf']['localBS']=obj.localBS

        # sanity checks
        # nada
        return obj
    
#...!...!..................
    @classmethod 
    def predictor(cls, args):
        print('Cnst:pred')
        obj=cls(**vars(args))
        obj.read_metaInp(obj.dataSplit)
        obj.load_hyperParams()                
        return obj        
# * * * *   alternative constructors   END * * * *

      
#...!...!..................        
    def load_hyperParams(self):  # used only by train?, yes?
        self.prjName2=self.prjName+'_'+self.modelDesign
        
        bulk=read_yaml(self.hparF,verb=self.verb)
        #print('input hpar',bulk)
        self.hparams=bulk

        if self.verb: print('read hyperParams:',self.hparF,'items:',len(self.hparams))
        # refine location of input data
        if self.dataSource not in  self.metaD['sourceDir']:
            inpDir=self.dataSource
            if self.myRank==0: print('use user defined data path:',inpDir)
        else:  # use shortcut stored in yaml file
            inpDir=self.metaD['sourceDir'][self.dataSource]
            #print('ee',inpDir,obj.metaD['subDir'])
            inpDir+=self.metaD['subDir']
        self.dataPath=inpDir
 

#...!...!..................        
    def read_metaInp(self,metaF):
        bulk=read_yaml(metaF,verb=self.verb)
        self.metaD=bulk
        if self.verb: print('read metaInp:',self.metaD.keys())
        self.metaD['metaFile']=metaF
        if self.verb>1: print('read metaInp dump',self.metaD)
        # update prjName if not 'format'
        #Xself.prjName2=self.prjName+'_'+self.modelDesign


#...!...!..................
    def init_dataLoaders(self):        
        dstConf={}
        for x in [ 'sampleNorm','h5nameTemplate']:
            dstConf[x]=self.metaD['dataInfo'][x]
        for x in ['inputShape','outputSize']:
            dstConf[x]=self.hparams[x]
        dstConf['numLocalSamples']=self.localSamples
        dstConf['dataPath']=self.dataPath
        dstConf['shuffle']=True  # use False for reproducibility
        dstConf['x_y_aux']=False
        dstConf['myRank']=self.myRank
        
        hpar=self.hparams
        localBS=hpar['train_conf']['localBS']
        numLocalCPU=hpar['train_conf']['numLocalCPU']
        
        if self.verb:
            print('D:init DataLoader(s), inpDir=%s, localBS=%d, localSamples='%(self.dataPath,localBS ),self.localSamples)
            
        self.sumRec['dataset']={}
        self.dataLoader={}
        for dom in ['train','val','train8']:
            dstConf['name']='DST-'+dom
            dstConf['domain']=dom
            
            if dom=='val': dstConf['numLocalSamples']//=8 # needs less data
            if dom=='train8': dstConf['domain']='train'  # there is squence dependence here
            if dom!='train8': dstConf['dataList']=self.metaD['dataList'][dom]
            
            inpDst=Dataset_CosmoFlow(dstConf,verb=self.verb)
            inpDst.sanity(localBS)
            self.dataLoader[dom]=DataLoader(inpDst, batch_size=localBS, shuffle=True, num_workers=numLocalCPU, pin_memory=True)            
            self.sumRec['dataset'][dom]=copy.deepcopy(dstConf)
        self.sumRec['state']='model_build'
        #1self.sumRec['XY_shapes']=self.inpGenD[dom].XY_shapes()
        # add input/output dimemnsions to hyper-params
                
        self.verb: print('seeded dataLoaders: ',self.dataLoader.keys(),', localBS=',localBS)

        rec3={}
        for dom in self.dataLoader:
            rec3[dom]={'steps': len(self.dataLoader[dom]),'events': len(self.dataLoader[dom].dataset)}
        #print('rec3',rec3)
        self.sumRec['dataset']=rec3
        
        if self.verb<=1: return
        
        print('\n print a fraction one batch of training data ')
        xx, yy,aux = next(iter(self.dataLoader[dom]))
        print(dom,'batch, X,Y;',xx.shape,yy.shape)
        print('Y[:10]',yy[:10])

            
#...!...!..................
    def build_model(self):
        if self.verb: print('build_model START, design=',self.modelDesign)
        model=CNNandFC_Model(self.hparams, verb=self.verb)

        if self.verb>1: print('myModel:',model)

        rec2=model.summary() #;    print(rec2)
        
        # create summary record
        rec={'hyperParams':self.hparams,
             'modelDesig' : self.modelDesign,
             'headNodeName' : self.headNodeName,
             'numRanks': self.numRanks,
             'myRank': self.myRank,
             'localRank': self.locRank,
        }
        self.sumRec.update(rec)        
        self.sumRec['modelSummary']=rec2
        self.model=model

        trainD=self.hparams['train_conf']
        # define loss function
        if trainD['lossName']=='mse':
            self.loss_func = torch.nn.MSELoss()# Mean Squared Loss
        else:
            fix_loss78
            
        if self.verb==0: return
        # only printout code below
        print('loss=',self.loss_func)
        
        from torchsummary import summary
        inpShp=self.hparams['inputShape'] # data must be opened 1st

        print('\n\nM: torchsummary.summary(model), inputShape=',inpShp)
                
        #summary(model,(inpShp[0],inpShp[1]), device='cpu')
        summary(model,tuple(inpShp), device='cpu')
        model.verb=0  #??

        return

#...!...!..................
    def train_model(self):
        if self.verb: print('train_model numRanks=%d history period=%d  START ...'%(self.numRanks,self.historyPeriod))

        hpar=self.hparams
        trainD=self.hparams['train_conf']
        
        self.model=self.model.to(self.device) # re-cast model on device, data will be casted later
        
            
        # Initialize optimizer
        optName, initLR=trainD['optimizer']
        currLR=initLR

        if optName=='adam' :
            self.optimizer=torch.optim.Adam(self.model.parameters(),lr=initLR)
        else:
            print('invalid Opt:',optName); exit(99)
        if self.verb: print('opt:',self.optimizer)

        lrSched=None
        if 'ReduceLROnPlateau' in hpar:
            patx,facx,coolx=hpar['ReduceLROnPlateau']
            lrSched=ReduceLROnPlateau(self.optimizer, mode='min', factor=facx, patience=patx, cooldown=coolx, verbose=True)
            if self.verb: print('enable ReduceLROnPlateau:',patx,facx,coolx)

        earlyStop=None
        if 'EarlyStopping' in hpar:
            paty, dely, wary, chkpty= hpar['EarlyStopping']
            earlyStop=EarlyStopping(patience=paty, verbose=self.verb, warmup=wary, chkptFile=self.outPath+'/'+self.prjName2+'.state_best.pth')
            if self.verb: print('enable EarlyStopping:',paty, dely, wary, chkpty)
            
        # set monitoring variables for traing
        self.sumRec['earlyStopOccured']=0
        histL=[]
        startT0=time.time()
        train_sum=0; kEpoch=0; startTM=time.time()
        if self.verb: print('start training over %d epochs'%self.maxEpochs)
        try_barrier(self.myRank,'train-start')

        for epoch in range( self.maxEpochs ):
            kEpoch+=1            
            doMon= (epoch<4) or (epoch%self.historyPeriod==0) or  (epoch==self.maxEpochs-1)
            startT1=time.time()
            wrk_loss=model_train( self)
            train_sum+=time.time() - startT1
            val_loss=model_infer( self, 'val')

            if  lrSched:
                currLR=self.optimizer.param_groups[0]['lr']
                lrSched.step(val_loss) # Decay Learning Rate

            if  earlyStop!=None and earlyStop(val_loss, self.model) :
                self.sumRec['earlyStopOccured']=1
                if self.verb: print("Early stopping, best val_loss=%.3g"%(-earlyStop.best_score))
                doMon=True

            if not doMon : continue
            # those operations cost more time , are mainly for monitoring purposes
            train_loss=model_infer(self,'train8')

            elaT=time.time() - startT0
            avr_sum=(time.time() - startTM)/kEpoch
            train_sum/=kEpoch
            
            if self.verb: print('epoch=%d loss wrk=%.3g, train=%.3g, val=%.3g, LR=%.3g, elaT=%.1f sec'%(epoch,wrk_loss,train_loss,val_loss,currLR,elaT))
            rec={'epoch':epoch,'train_loss':train_loss,'val_loss':val_loss,
                 'wrk_loss':wrk_loss,'lr':currLR,
                 'run_time':{'elapsed':elaT,'train_epoch':train_sum,'avr_epoch':avr_sum}
            }
            histL.append(rec)
            if self.sumRec['earlyStopOccured']==1: break
            # clear some of counters
            startTM=time.time()
            kEpoch=0
            train_sum=0

        #. . . .  end of epochs . . . . . .
        trainTime=time.time() - startT0
        trainEpochs=histL[-1]['epoch'] - histL[0]['epoch']
        trainFrames=trainEpochs * self.sumRec['dataset']['train']['events']

        self.sumRec['history']=histL
        self.sumRec['device']=self.device
        self.sumRec['trainTime']=trainTime
        self.sumRec['trainEpochs']=trainEpochs
        self.sumRec['trainFrames']=trainFrames
               
        end_loss=histL[-1]['val_loss']
        
        if self.verb: print('\n End Val Loss:%.3f  train: epochs=%d time/min=%.1f frames=%.1fk'%(end_loss,trainEpochs,trainTime/60.,trainFrames/1000.))
 
        return    
 
    #............................
    def save_model_full(self):
        outF=self.outPath+'/'+self.prjName2+'.model.tar'
        epochs=self.sumRec['trainEpochs']
        save_model_full(outF,self.model,self.optimizer,epochs)

#...!...!....................
    #............................
    def load_model_state(self):
        start = time.time()
        stateF=self.seedWeights+'/'+self.prjName2+'.state_best.pth'
        #stateF=self.seedWeights+'/'+self.prjName2+'.model.tar'
        print('load  weights  from',stateF,end='... ')
        if not os.path.exists(stateF): 
            print('Aborting on start, missing  model:',stateF)
            exit(99)
       
        xx=torch.load(stateF,map_location=torch.device('cpu'))
        self.model.load_state_dict(xx)
        self.model=self.model.to(self.device) # re-cast model on device, data will be casted later
        print('loaded, elaT=%.2f sec'%(time.time() - start))

  
