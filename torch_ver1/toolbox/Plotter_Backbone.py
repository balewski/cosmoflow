import os
import numpy as np

import socket  # for hostname
import time

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_Backbone(object):
#...!...!....................
    def __init__(self, args):
        if args.noXterm:
            print('disable Xterm')
            import matplotlib as mpl
            mpl.use('Agg')  # to plot w/o X-server
        import matplotlib.pyplot as plt
        print(self.__class__.__name__,':','Graphics started')
        plt.close('all')
        self.plt=plt
        self.figL=[]
        self.outPath=args.outPath
        self.prjName=args.prjName
       
#...!...!....................
    def display_all(self,ext,pdf=1):
        if len(self.figL)<=0: 
            print('display_all - nothing top plot, quit')
            return
        if pdf:
            for fid in self.figL:
                self.plt.figure(fid)
                self.plt.tight_layout()                
                figName='%s/%s_%s_f%d.png'%(self.outPath,self.prjName,ext,fid)
                print('Graphics saving to %s  ...'%figName)
                self.plt.savefig(figName)
        self.plt.show()

# figId=self.smart_append(figId)
#...!...!....................
    def smart_append(self,id): # increment id if re-used
        while id in self.figL: id+=1
        self.figL.append(id)
        return id

#...!...!....................
    def train_history(self,deep,args,figId=10):

        # pre-process training history
        trecL=deep.sumRec['history']
        print('plot_train_history: got %d train records'%len(trecL))

        localBS=deep.sumRec['hyperParams']['train_conf']['localBS']
        trainSteps=deep.sumRec['dataset']['train']['steps']
        trainEve=deep.sumRec['dataset']['train']['events']
        train8Eve=deep.sumRec['dataset']['train8']['events']
        valEve=deep.sumRec['dataset']['val']['events']
        
        trecEnd=trecL[-1]
        nEpochs=trecEnd['epoch']
        valLoss=trecEnd['val_loss']
        elapsedT=trecEnd['run_time']['elapsed']

        epoV=[] # epochs for which we have data
        VD={'train_fit':([],':bx',' n=%d'%trainEve),
            'train':([],'--bo',' n=%d'%train8Eve),
            'val':([],'--rd',' n=%d'%valEve),
        }

        hasLR=False
        if 'lr' in trecEnd:
            hasLR=True ;  lrV=[]
        
        speedD={'train_only':[],'agreg_epoch':[]}
        for rec in trecL:
            epoV.append(rec['epoch'])
            VD['train_fit'][0].append(rec['wrk_loss'])
            VD['train'][0].append(rec['train_loss'])
            VD['val'][0].append(rec['val_loss'])
            speedD['train_only'].append( trainEve/rec['run_time']['train_epoch'])
            speedD['agreg_epoch'].append( trainEve/rec['run_time']['avr_epoch'])
            #delTD['elaT'].append( rec['run_time']['elapsed']) # not used
            if hasLR:   lrV.append(rec['lr'])
            
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,4))

        nrow,ncol=3,3
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=2, rowspan=2 )
        tit1='%s, train %.2f min, end-val=%.3g'%(deep.prjName2,elapsedT/60.,valLoss)
        ax1.set(ylabel='loss = %s'%deep.hparams['train_conf']['lossName'],title=tit1,xlabel='epochs, earlyStopOccured=%d'%deep.sumRec['earlyStopOccured'])
        for key in VD:
            ax1.plot(epoV,VD[key][0],VD[key][1],label=key+VD[key][2])

        ax1.legend(loc='lower left')
        ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')

 
        # plot time per frame
        ax2  = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=1, rowspan=2 )
        tmpD={}        
        for key in ['agreg_epoch','train_only']:  # start with larger values
            ax2.hist(speedD[key],bins=10,label=key, alpha=0.7)
            zmu=np.array(speedD[key])
            resM=float(zmu.mean())
            resS=float(zmu.std())
            tmpD[key]=( resM, resS )
            print('avr speed key=%s  %.1f +/- %.1f frame/sec'%(key,resM, resS ))
        overHead=tmpD['train_only'][0]/ tmpD['agreg_epoch'][0]
        print('Cpu overhead due to validation=%.2f'%overHead)
        ax2.legend(loc='upper right')

        numRanks=deep.sumRec['numRanks']
        globSpeed=tmpD['agreg_epoch'][0]*numRanks
        globSpeedErr=tmpD['agreg_epoch'][1]*numRanks
        
        ax2.set(title='glob samp/sec %.1f+/-%.1f'%(globSpeed,globSpeedErr), xlabel='global frames/sec ')
        txt=''
        txt+='device=%s \nmyRank %d of %d\n'%(deep.sumRec['device'],deep.sumRec['myRank'],numRanks)
        txt+='steps=%d\n'%trainSteps
        txt+='epochs=%d\n'%nEpochs
        txt+='train localBS=%d\n'%localBS
        txt+='val overhead=%.2f'%overHead
        
        x0=0.2
        ax2.text(x0,0.2,txt,transform=ax2.transAxes)
       
        if  not hasLR : return
        ax3 = self.plt.subplot2grid((nrow,ncol), (2,1), colspan=2,sharex=ax1 )

        ax3.plot(epoV,lrV,'.-',label='learn rate')
        ax3.legend(loc='best')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set_yscale('log')
        ax3.set(ylabel='learning rate')
   
