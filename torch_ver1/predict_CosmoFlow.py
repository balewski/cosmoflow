#!/usr/bin/env python
""" 
read input hd5 tensors
read trained net : model+weights
read test data from HD5
infere for  test data 
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"
import numpy as np
import  time
import sys,os
sys.path.append(os.path.relpath("toolbox/"))
from Plotter_SquareRegr import Plotter_SquareRegr
from Deep_SquareRegr import Deep_SquareRegr
from Dataset_SquareRegr  import Dataset_SquareRegr
from torch.utils.data import  DataLoader

from Util_Torch import model_infer
from Util_SquareRegr import  get_UmZ_correl
from Util_IOfunc import read_yaml, write_yaml

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--design', dest='modelDesign',  default='cnn1', help=" model design of the network")
    parser.add_argument("--dataTag",  default='december5',help="data config name")
    parser.add_argument("--dom",default='train', help="domain is the dataset for which predictions are made",choices=['train','test','val'])

    parser.add_argument('--device',  default='cpu', choices=['cpu','cuda'],help=' hardware to be used, multi-gpus not supported')

    parser.add_argument("-designPath",help="location of hpar.yaml defining the model",  default='./')

    parser.add_argument("--dataPath",help="path to input",default='data3/')

    parser.add_argument("--seedWeights", default='same', help="seed weights only, after model is created")

    parser.add_argument("-o", "--outPath", default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm', action='store_true', default=False, help="disable X-term for batch mode")

    parser.add_argument("-n", "--localSamples", type=int, default=2000, help="samples to read")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2], help="increase output verbosity", default=1, dest='verb')
    args = parser.parse_args()
    args.prjName='squareRegr'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.seedWeights=='same' :  args.seedWeights=args.outPath

deep=Deep_SquareRegr.predictor(args)
plot=Plotter_SquareRegr(args,deep.metaD  )

#1plot.frames(deep.data[dom]); plot.display_all('predict')  

deep.build_model()
deep.load_model_state()

dom=args.dom
dstConf={}
for x in [ 'dataList','h5nameTemplate','inputShape','outputSize']:
    dstConf[x]=deep.metaD['dataInfo'][x]
dstConf['numLocalSamples']=args.localSamples
dstConf['dataPath']=deep.dataPath
dstConf['shuffle']=True  # use False for reproducibility
dstConf['x_y_aux']=True # it will produce 3 np-arrays per call
dstConf['name']='DST-'+dom
dstConf['domain']=dom
dstConf['myRank']=12

print('\nM:Create torch-Dataset instance')
#pprint(dstConf)
inpDst=Dataset_SquareRegr(dstConf,verb=2)
localBS=8
numLocalCPU=2  # -c10 locks 5 physical cpu cores
inpDst.sanity(localBS)

print('\nCreate torch-DataLoader instance & test it')
trainLdr = DataLoader(inpDst, batch_size=localBS, shuffle=True, num_workers=numLocalCPU, pin_memory=True)
deep.dataLoader={dom: trainLdr}

print('sample predictions:')
loss,X1,Y1,Z1=model_infer( deep,dom,outType=1)

print('quick infer : Average loss: %.4f  events=%d '% (loss,  Z1.shape[0]))
for i in range(Z1.shape[0]):
    print('y=',Y1[i],'   z=',Z1[i],'   diff=',Y1[i]-Z1[i])
    if i>3: break
dataA={dom+'_image_X':X1,dom+'_unit_Y':Y1,dom+'_unit_Z':Z1}
plot.frames(dataA,dom) #; gra.display_all(args,'predict')  

print('\nfull test predictions:')
startT=time.time()
loss,Y2,Z2=model_infer( deep,dom,outType=2)
predTime=time.time()-startT
print(' infer : Average loss: %.4f  events=%d , elaT=%.2f min'% (loss,  Z2.shape[0],predTime/60.))

lossMSE=get_UmZ_correl(Y2-Z2,deep.metaD)
sumRec={}
sumRec[dom+'LossMSE']=float(lossMSE)
sumRec['predTime']=predTime
sumRec['numSamples']=Y2.shape[0]
sumRec['design']=deep.modelDesign
write_yaml(sumRec, deep.outPath+'/'+deep.prjName+'.sum_pred.yaml')

tit='dom=%s '%('val')
plot.fparam_residu(Y2,Z2, figId=9,tit=tit, tit2='loss=%.3f'%loss)
plot.data_fparams(Z2,'pred Z',figId=8)
plot.data_fparams(Y2,'true Y',figId=7)

plot.display_all('predict')  
