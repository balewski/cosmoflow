#!/usr/bin/env python
"""
Runs on CPU,  1 GPU, or multipl GPUs
Uses TCP/IP for communication
read input hd5 tensors in to RAM

Testing:
on CPUs: 30 sec/epoch
./train_CosmoFlow.py --dataSplit cosmo8_dataSplit_small.yaml --localSamples 16 --localBS 2  


Interactive 2x2 GPU:
   salloc -N2  -C gpu  -c 10  --gres=gpu:2 --ntasks-per-node=2  -t4:00:00 

   line=`ifconfig |grep 'ib1:' -A1 |tail -n1`
   headIp=`echo $line | cut -f2 -d\ `
   export MASTER_ADDR=$headIp
   export MASTER_PORT=8888
   echo headIP=$headIp

srun -n2 -l python -u ./train_CosmoFlow.py --dataSplit cosmo8_dataSplit_small.yaml --localSamples 16 --localBS 2  --device cuda  
GPU time ~ 4 sec/epoch

   run on 1-8 GPUs preserving total samples and global BS:


Predict:
      srun -n1 -l python -u ./predict_CosmoFlow.py --device cuda ??


"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from pprint import pprint 

import sys,os
sys.path.append(os.path.relpath("toolbox/"))
from Plotter_CosmoFlow import Plotter_CosmoFlow
from Deep_CosmoFlow import Deep_CosmoFlow
from Util_IOfunc import  read_yaml,write_yaml


import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--design', dest='modelDesign',  default='a1_e5bb4b43', help=" model design of the ML-network")
    parser.add_argument('--device',  default='cpu', choices=['cpu','cuda'],  help=' hardware to be used')
    parser.add_argument("--seedWeights", default=None, help="seed weights only, after model is created") # not tested

    parser.add_argument("--designPath",help="location of hpar.yaml defining the model",  default='./')

    parser.add_argument("--outPath", default='out',help="output path for plots and tables")

    parser.add_argument("--dataSource",type=str, default='csc',help="path to data vault: cscratch or CFS")

    parser.add_argument("--dataSplit",default='cosmo8_dataSplit_128cube12chan.yaml',help="how to split data onto train/val/test")


    parser.add_argument("--localSamples", type=int, default=2000, help="samples to read")

    parser.add_argument("-e", "--epochs", type=int, default=5, dest='maxEpochs', help="max training  epochs assuming localSamples*numRanks per epoch")

    parser.add_argument("--localBS", type=int, default=None, help="change localBS, default --> hparams")

    parser.add_argument("-H", "--historyPeriod", type=int, default=5, help="in epochs, compute & save train+val loss.")

    parser.add_argument('-X', "--no-Xterm", dest='noXterm',  action='store_true', default=False, help="disables X-term for batch mode")
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],  help="increase output verbosity", default=1, dest='verb')

    args = parser.parse_args()
    args.prjName='cosmoFlow'
    if args.device!='cuda':
        for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
if args.seedWeights=='same' :  args.seedWeights=args.outPath

deep=Deep_CosmoFlow.trainer(args)
deep.init_dataLoaders()

if deep.myRank==0  : 
    plot=Plotter_CosmoFlow(args ,deep.metaD )
    dom='val'
    xx, yy, _ = next(iter(deep.dataLoader[dom]))
    
deep.build_model()

if args.seedWeights: deep.load_model_state()
deep.train_model()
if deep.myRank!=0:  exit(0)  # stop all other ranks here

deep.save_model_full() 
write_yaml(deep.sumRec, deep.outPath+'/'+deep.prjName2+'.sum_train.yaml')

if args.verb>1: pprint(deep.sumRec)

plot.train_history(deep,args) 
plot.display_all('train')
