#!/usr/bin/env python
""" 
read input hd5 tensors
train net
write net + weights as HD5

Mutli-scale data set training

To check it runs (at all ) on Cori CPUs,  no Horovod

module load tensorflow/gpu-2.2.0-py37


* * *   GPU  executions on Cori
ssh cori 
module load esslurm
module load  tensorflow/gpu-2.1.0-py37

**** CPU execution (1 min to load + 1 min/epoch)
./train_CosmoFlow.py --noHorovod    --localSamples 16 --dataSplit cosmo7_dataSplit_small.yaml --epochs 5  --localBS 2


****** few GPUs
salloc  -C gpu -n1 -c 10 --gres=gpu:1  -t2:00:00 

2 GPUs:
salloc -C gpu -N 1 -t 4:00:00 -c 10 --gres=gpu:2 --ntasks-per-node=2 

srun -n1 bash -c '  nvidia-smi '
srun  -n 2 bash -c "  python -u train_CosmoFlow.py ..."
CosmoFlow speed gl avr:11.0 std:3.6 (fr/sec), global, 5 epochs 


******** 2GPU on DGX, must use shifter image
Use Shifter image for DGX
salloc -C dgx -N 1 -t 4:00:00 -c 10 --gres=gpu:2 --ntasks-per-node=2  -w cgpu19 

srun -n1  shifter --image nersc/tensorflow:ngc-20.09-tf2-v0   bash -c "  python -u ./train_CosmoFlow.py  --localSamples 16 --dataSplit cosmo7_dataSplit_small.yaml --epochs 5  --localBS 2 "

echo inShifter:`env|grep  SHIFTER_RUNTIME`


* * *   GPU  executions on Summit
ssh summit
module load ibm-wml-ce

**** login-node GPU execution
it may need   export CUDA_VISIBLE_DEVICES=2 
 ./train_CosmoFlow.py --localSamples 16 --dataSplit cosmo7_dataSplit_small.yaml --epochs 3  --localBS 2 --dataSource smt  --design lbann2  --noHorovod  -X

*** GPU: use batch .lsf scripts

"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import sys,os
sys.path.append(os.path.abspath("toolbox"))

from Plotter_CosmoFlow import Plotter_CosmoFlow
from Deep_CosmoFlow import Deep_CosmoFlow
from Util_IOfunc import write_yaml

import argparse
def get_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],  help="increase output verbosity", default=1, dest='verb')
    
    parser.add_argument( "--noHorovod", dest='useHorovod',  action='store_false', default=True, help="disable Horovod to run on 1 node on all CPUs")

    parser.add_argument('--design', dest='modelDesign', default='lbann2',help=" model design of the network")

    parser.add_argument("--seedWeights",  default=None, help="seed weights only, after model is created")

    parser.add_argument("-o","--outPath", default='out',help="output path for plots and tables")

    parser.add_argument("--dataSource",type=str, default='cfs',help="path to data vault: cscratch or CFS")

    parser.add_argument("--dataSplit",default='cosmo7_dataSplit_128cube12chan.yaml',help="how to split data onto train/val/test")

    parser.add_argument("-e", "--epochs", type=int, default=5, dest='goalEpochs', help="max training  epochs assuming localSamples*numRanks per epoch")

    parser.add_argument("-b", "--localBS", type=int, default=16,  help="training local BS, default:hpar[]")

    parser.add_argument( "--localSamples", type=int, required=True,  help="samples per worker, it defines 1 epoch")
    
    parser.add_argument( "--warmUpEpochs", type=int, default=None, help="for multi-GPU job, overwrite default:hpar[]")

    parser.add_argument("--initLRfactor", type=float, default=1., help="scaling factor for initial LRdrop")
    parser.add_argument("--dropFrac", type=float, default=None, help="drop fraction at all FC layers, default:hpar[]")

    parser.add_argument( "-s","--earlyStop", type=int, dest='earlyStopPatience', default=80, help="early stop:  epochs w/o improvement (aka patience), 0=off")

    parser.add_argument( "--checkPt", dest='checkPtOn', action='store_true',default=False,help="enable check points for weights")

    parser.add_argument("-j","--jobId", default=None, help="aux info to be stored w/ summary")
    parser.add_argument( "-X","--noXterm", dest='noXterm',  action='store_true', default=False, help="disable X-term for batch mode")


    args = parser.parse_args()
    args.shuffle_data=True # use False only for debugging
    args.prjName='cosmoFlow'
    if args.seedWeights=='None': args.seedWeights=None
    if not args.useHorovod:
        for arg in vars(args): print( 'myArg:',arg, getattr(args, arg))

    return args


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

deep=Deep_CosmoFlow.trainer(args)
if deep.myRank==0:
    plot=Plotter_CosmoFlow(args,deep.metaD  )

#1 gra.plot_data_fparams(deep.data['val'][1],'true U',figId=6)
deep.init_genetors() # before building model, so data dims are known
deep.build_model()
sumF=deep.outPath+'/'+deep.prjName+'.sum_train.yaml'

if args.seedWeights=='same' :  args.seedWeights=args.outPath
if args.seedWeights:
    deep.load_weights_only(path=args.seedWeights)


#1 ppp.plot_model(deep,1) # depth:0,1,2
if args.goalEpochs >5 and deep.myRank==0: 
    deep.save_model_full() # just to get binary dump of the graph
    write_yaml(deep.sumRec, sumF)    

deep.train_model() 

if deep.myRank>0: exit(0)

deep.save_model_full()
plot.train_history(deep,figId=100+deep.myRank)
try:
    a=1

except:
    deep.sumRec['plots']='failed'
    print('M: plot.train_history failed')

write_yaml(deep.sumRec, sumF)

plot.display_all('train')


