__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

'''
reads all data at once and serves them from RAM
- optimized for mult-GPU training w/ Horovod
- only uses a block of data  from each h5-file
- each GPU reads some data from all h5-files
    Generates data for Keras.fit
    Keep all data in RAM
    Reads block of data from files only in constructor
    Shuffle: only  all samples after compleated read
    Allow Keras.fit to requests for random batches
'''

import time,  os
import random
import h5py
import numpy as np
import copy

from tensorflow.keras.utils import Sequence
import tensorflow.keras.backend as K
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class Cosmo7_input_generator(Sequence):

#...!...!..................
    def __init__(self, conf0, verb=1):
        self.conf=copy.deepcopy(conf0)  # the input conf0 is reused later in the upper level code
        self.verb=verb
        self.stepTime =[] # history of completed epochs      
        self.cnt={'epoch':0,'step':0,'file':0}
        self.batch_size=self.conf['localBS']

        fileIdxL=self.conf['fileIdxList']
        numFile=len(fileIdxL)
        numPar=self.conf['numPar']
        assert numFile>=1
        self.conf['numSamplesPerFile']=self.conf['numLocalSamples']//numFile
        self.numLocFrames=self.conf['numSamplesPerFile']*numFile # can be < numLocalSamples
        assert self.conf['numSamplesPerFile']>=1
        self.stepPerEpoch=int(np.floor( self.numLocFrames/ self.batch_size))
        if  self.stepPerEpoch <1:
            print('\nABORT1, Have you requested too few samples per rank?, numLocFrames=%d, BS=%d  name=%s'%(self.numLocFrames, self.batch_size,self.conf['name']))
            exit(67)

        if self.verb:
            print('\ngen-cnst name=%s  shuffle=%r localBS=%d steps=%d myRank=%d numSampl/File=%d numFile=%d'%(self.conf['name'],self.conf['shuffle'],self.batch_size,self.__len__(),self.conf['myRank'],self.conf['numSamplesPerFile'],numFile),'H5-path=',self.conf['dataPath'])
        assert self.numLocFrames>0
        assert self.conf['myRank']>=0

        if self.verb>1:
            print('IG: use %d fileIdx:'%(len(fileIdxL)),fileIdxL)

        dataShape=copy.copy(self.conf['sampleShape'])
        dataShape.insert(0,self.numLocFrames)
        # malloc storage for the data, to avoid concatenation
        self.data_frames=np.zeros(tuple(dataShape),dtype='float32')
        self.data_parU=np.zeros((self.numLocFrames,numPar),dtype='float32')
        if self.conf['x_y_aux']:
            self.data_parP=np.zeros((self.numLocFrames,numPar),dtype='float32')

        idxA=np.arange(self.numLocFrames)
        # RAM-efficient pre-shuffling of target indexes
        if self.conf['shuffle']:
            np.random.shuffle(idxA)
            if self.verb: print('IG:pre-shufle all local %d frames'%self.numLocFrames)

        # prime this generator  ... takes few sec/file
        startTm0 = time.time()
        for ic in range(numFile):
            numSamp=self.conf['numSamplesPerFile']
            idxOff=ic*numSamp
            goalIdxL=idxA[idxOff:idxOff+numSamp]
            pr=self.verb>0 and (ic<10 or ic%20==0)
            self.openH5(fileIdxL[ic],goalIdxL,pr)

        startTm1 = time.time()

        if self.verb :
            print('IG:load-end of all HD5, read time=%.2f(sec) name=%s numFile=%d, numLocSamp=%d, numLocSamp/file=%d'%(startTm1 - startTm0,self.conf['name'],numFile,self.numLocFrames,self.conf['numSamplesPerFile']))
            print('IG:Xall',self.data_frames.shape,self.data_frames.dtype)
            print('IG:Uall',self.data_parU.shape,self.data_parU.dtype)
            if self.conf['x_y_aux']:
                print(' IG:Pall',self.data_parP.shape,self.data_parP.dtype)

        if 0: #compute means and std_dev, must be done as F64 for accuracy of STD, takes *additional* 2x more RAM for storing 'Ff64'
            print('\n check mean & std - it will take long time, X:',self.data_frames.shape)
            Xf64=self.data_frames.astype('double')
            axt=(0,1,2,3)
            avrX=Xf64.mean(axis=axt)
            stdX=Xf64.std(axis=axt)
            np.set_printoptions(precision=3)
            print('X avr=',avrX.shape,avrX)
            print('X std=',stdX.shape,stdX)
            #print('X avr=%.4f, \nstd=%.4f'%(avrX,stdX))
            exit(0) # thos code is used only for testing
                
#...!...!..................
    def openH5(self,fileIdx,goalIdxL,pr):
        fnameTmpl=self.conf['dataPath']+self.conf['h5nameTemplate']
        inpF=fnameTmpl.replace('*',str(fileIdx))
        numSamp=self.conf['numSamplesPerFile']
       
        if self.verb>1 : print('IG:fileIdx %d name=%s, idxOff[0..3]='%(fileIdx,self.conf['name']),goalIdxL[:3],'inpF=',inpF)

        doAux= self.conf['x_y_aux']
        meanX=self.conf['sampleNorm']['mean']
        stdX=self.conf['sampleNorm']['std']
        time0 = time.time()

        if pr :
            print('IG:read numSamp=%d  hdf5:%s'%(numSamp,inpF),', doAux=%r '%(doAux))
        assert  os.path.exists(inpF)
        self.cnt['file']+=1
        # = = = READING HD5  start
        h5f = h5py.File(inpF, 'r')
        Xshape=h5f['3Dmap'].shape
        totSamp=Xshape[0]
        maxShard=totSamp//numSamp
        if  maxShard <1:
            print('\nABORT, Have you requested too many samples per rank?, one file Xshape:',Xshape,'name=',self.conf['name'],'numSamp=',numSamp)
            exit(66)
        # chosen shard is rank dependent, wraps up if not sufficient number of ranks        
        myShard=self.conf['myRank'] %maxShard
        sampIdxOff=myShard*numSamp
        if pr: print(' IG:file myShard=%d, maxShard=%d, sampIdxOff=%d, file:'%(myShard,maxShard,sampIdxOff),'Xshape:',Xshape)

        assert len(meanX)==Xshape[4]
        assert len(stdX)==Xshape[4]
        # data reading starts
        # .......................................................
        #.... data embeddings, transformation should go here ....

        self.data_frames[goalIdxL]=(h5f['3Dmap'][sampIdxOff:sampIdxOff+numSamp]-meanX)/stdX # do normalization in-fly, it tripples read time/file
        self.data_parU[goalIdxL]=h5f['unitPar'][sampIdxOff:sampIdxOff+numSamp]
        if doAux:
            self.data_parP[goalIdxL]=h5f['physPar'][sampIdxOff:sampIdxOff+numSamp]
        
    #...!...!..................
    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.stepPerEpoch

#...!...!..................
    def __getitem__(self, input_index):
        'use input index to assure good performance'

        #print('GI:',input_index,self.conf['name'])
        bs=self.batch_size
        pCnt=input_index*bs

        # WARN : do NOT do data embeddings here
        # (most likely) this code is not thread-safe and works well (by accident) if the code below takes ~0 time

        X=self.data_frames[pCnt:pCnt+bs]
        Y=self.data_parU[pCnt:pCnt+bs]

        # hack for TF2.1 who is not calling on_epoch_end() any more
        self.cnt['step']+=1
        # this iss needed only for 1GPU & noHorovod training - lets keep it 
        if self.cnt['step']%self.__len__()==0: self.on_epoch_end2()

        if self.conf['x_y_aux']: # predictions for Roy
            AUX=self.data_parP[pCnt:pCnt+bs]
            return (X,Y,AUX)

        return (X,Y, [None])


#...!...!..................
    def XY_shapes(self):
        return {'X':list(self.data_frames.shape[1:]),'Y': list(self.data_parU.shape[1:] )}

    #...!...!..................
    def on_epoch_end(self): # used when TF is not calling end-of-epch method by itself
        #if self.verb: print('XXX Updates history after each epoch',self.conf['name'],self.cnt)
        a=1

    #...!...!..................
    def on_epoch_end2(self):
        'Updates history after each epoch'
        if self.cnt['epoch']%10==0 and self.verb:
                print('GEN:%s on_epoch_end cnt:'%self.conf['name'],self.cnt)
        self.cnt['epoch']+=1
        timeRec=[time.time(), self.cnt['epoch']]
        self.stepTime.append(timeRec)

