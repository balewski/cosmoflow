import numpy as np

from matplotlib import cm as cmap
from Util_Func import get_correlation

from Plotter_Backbone import Plotter_Backbone
from mpl_toolkits.mplot3d import Axes3D

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_CosmoFlow(Plotter_Backbone):
    def __init__(self, args,metaD):
        Plotter_Backbone.__init__(self,args)

        self.maxU=1.3
        self.metaD=metaD

#............................
    def plot_model(self,deep,flag=0):
        #print('plot_model broken in TF=1.4, skip it'); return

        if 'cori' not in socket.gethostname(): return  # software not installed
        model=deep.model
        fname=self.outPath+'/'+deep.prjName+'_graph.svg'
        plot_model(model, to_file=fname, show_layer_names=flag>0, show_shapes=flag>1)
        print('Graph saved as ',fname,' flag=',flag)

            
#............................
    def train_history(self,deep,figId=10):
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(9,5.5))
        
        nrow,ncol=4,3
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        ax1 = self.plt.subplot2grid((nrow,ncol), (0,1), colspan=2, rowspan=2 )
        
        DL=deep.train_hirD
        nEpochs=len(DL['val_loss'])
        localBS=deep.hparams['train_conf']['localBS']
        numRanks=deep.sumRec['numRanks']
        trainSampl=localBS*deep.inpGenD['train'].__len__()
        valSampl=localBS*deep.inpGenD['val'].__len__()

        stepTL=DL['stepTimeHist']
        delTLfr=[]; epoCntL=[]; delTLep=[];
        t0=None
        glob_epoch_size=localBS * deep.sumRec['steps_per_epoch']*numRanks       
        for t, eCnt in stepTL:  
            if t0==None:
                t0=t; continue
            delT_step=(t-t0)
            t0=t
            frm2sec=glob_epoch_size/delT_step
            delTLfr.append(frm2sec)
            delTLep.append(delT_step)
            epoCntL.append(eCnt)

        ple=len(DL['val_loss'])-1
        epoCntL=epoCntL[-ple:]
        delTLep=delTLep[-ple:]
        print('gg',eCnt,len(stepTL),'L:',len(epoCntL),len(DL['val_loss']),len(DL['loss']))

        valLoss=DL['val_loss'][-1]
        tit1='%s, train %.1f min, end-val=%.3g'%(deep.prjName,deep.train_sec/60.,valLoss)
        ax1.set(ylabel='loss = %s'%deep.hparams['train_conf']['lossName'],title=tit1,xlabel='epochs, earlyStopOccured=%d'%deep.sumRec['earlyStopOccured']) 
        ax1.plot(epoCntL,DL['loss'][1:],'b:',label='train fit locN=%d'%trainSampl)
        ax1.plot(epoCntL,DL['val_loss'][1:],'r.-',label='val locN=%d'%valSampl)

        if 'train_loss' in DL:
            ax1.plot(epoCntL,DL['train_loss'][1:],'b--',label='train epochEnd')    

        legTit=''
        if deep.jobId : legTit='job:'+deep.jobId
        ax1.legend(loc='best',title=legTit)
        ax1.set_yscale('log')
        ax1.grid(color='brown', linestyle='--',which='both')
        
        # time per frame
        ax2b  = self.plt.subplot2grid((nrow,ncol), (0,0), colspan=1, rowspan=2 )
        ax3 = self.plt.subplot2grid((nrow,ncol), (3,1), colspan=2,sharex=ax1 )
        ax3b  = self.plt.subplot2grid((nrow,ncol), (2,0), colspan=1, rowspan=1 )
        
        # fig .... frames/sec
        ax2b.hist(delTLfr,bins=30)
        zmu=np.array(delTLfr)
        resM=float(zmu.mean())
        resS=float(zmu.std())
        deep.sumRec['glob_frame_per_sec']=[ resM, resS ]
        numOpenFiles=deep.sumRec['num_open_files']

        tit2='gl avr:%.1f std:%.1f (fr/sec)'%(resM,resS)
        print('CosmoFlow speed %s, global, %d epochs'%(tit2,nEpochs))
        ax2b.set(title=tit2, xlabel='global sample/sec')
        x0=0.1
        ax2b.text(x0,0.85,'loc BS=%d'%localBS,transform=ax2b.transAxes)
        ax2b.text(x0,0.75,'loc steps=%d'%DL['steps_per_epoch'],transform=ax2b.transAxes)
        ax2b.text(x0,0.65,'myRank=%d of %d'%(deep.myRank,deep.numRanks),transform=ax2b.transAxes)

        txt3='loc files=%d\nepoch size=%d\nepochs=%d'%(numOpenFiles,glob_epoch_size,eCnt)
        ax2b.text(x0,0.35,txt3,transform=ax2b.transAxes)
        ax2b.axvline(resM, color='k', linestyle='--')

        # fig .... time per epoch
        zmu=np.array(delTLep)
        resM=float(zmu.mean())
        resS=float(zmu.std())

        ax3.plot(epoCntL,delTLep,'.-',c='g',label='epoch duration')
        ax3.grid(color='brown', linestyle='--',which='both')
        ax3.set(ylabel='(sec)')
        ax3.legend(loc='best')
        ax3.set_ylim(0,)
        ax3.axhline(resM, color='green', linestyle=':')

        # fig ....
        ax3b.hist(delTLep,facecolor='g',bins=30)
        deep.sumRec['glob_epoch_duration_sec']=[ resM, resS ]
       
        tit3=' avr epoch:%.2f std:%.2f (sec)'%(resM,resS)
        ax3b.set(title=tit3, xlabel='sec/epoch')
        ax3b.axvline(resM, color='green', linestyle=':')
 
        if 'lr' not in DL: return

        ax2 = self.plt.subplot2grid((nrow,ncol), (2,1), colspan=2,sharex=ax1 )
        # fig ....
        ax2.plot(epoCntL,DL['lr'][1:],'.-',label='learning rate')
        ax2.grid(color='brown', linestyle='--',which='both')
        ax2.set(title='model design='+deep.sumRec['modelDesign'])
        ax2.set_yscale('log')
        ax2.legend(loc='best')
   
#............................
    def physParam_residu(self,U,Z, figId=9,tit='',tit2='',tit3=''):

        colMap=cmap.rainbow            
        parName=self.metaD['parName']
        nPar=self.metaD['numPar']

        nrow,ncol=3,nPar
        #  grid is (yN,xN) - y=0 is at the top,  so dum
        figId=self.smart_append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(ncol*3.2,nrow*2.6))
        assert U.shape[1]==Z.shape[1]

        j=0
        for iPar in range(0,nPar):
            j=iPar
            jOff=1
            ax1 = self.plt.subplot(nrow,ncol, j+jOff); ax1.grid()
            ax2 = self.plt.subplot(nrow,ncol, j+ncol*1+jOff); ax2.grid()

            u=U[:,iPar] 
            z=Z[:,iPar]
            unitStr='(a.u.)'
            mm2=self.maxU
            mm1=-mm2
            mm3=self.maxU/2.  # adjust here of you want narrow range for 1D residues
            binsX=np.linspace(mm1,mm2,30)
            rho,sigRho=get_correlation(u,z)
            zsum,xbins,ybins,img = ax1.hist2d(z,u,bins=binsX, cmin=1,
                                              cmap = colMap)
            # beutification
            self.plt.colorbar(img, ax=ax1)
            ax1.plot([0, 1], [0,1], color='magenta', linestyle='--',linewidth=1,transform=ax1.transAxes) #diagonal
            # 

            ax1.set(title='%d:%s'%(iPar,parName[iPar]), xlabel='pred %s'%unitStr, ylabel='true')
            ax1.text(0.2,0.1,'rho=%.3f +/- %.3f'%(rho,sigRho),transform=ax1.transAxes)
            
            if iPar==0: ax1.text(0.1,0.92,tit,transform=ax1.transAxes)
            if iPar==1: ax1.text(0.1,0.92,'n=%d'%(u.shape[0]),transform=ax1.transAxes)
            if iPar==2: ax1.text(0.1,0.92,tit2,transform=ax1.transAxes)
            if iPar==3: ax1.text(0.1,0.92,tit3,transform=ax1.transAxes)
            
            # .... compute residue
            zmu=z-u
            resM=zmu.mean()
            resS=zmu.std()
 
            # ..... 1D residue
            binsU= np.linspace(-mm3,mm3,50)
            ax2.hist(zmu,bins=binsU)

            ax2.set(title='res: %.2g+/-%.2g'%(resM,resS), xlabel='pred-true %s'%unitStr, ylabel='frames')
            ax2.text(0.15,0.8,parName[iPar],transform=ax2.transAxes)


            ax3 = self.plt.subplot(nrow,ncol, j+ncol*2+jOff); ax3.grid()

            # ......  2D residue
            zsum,xbins,ybins,img = ax3.hist2d(z,zmu,bins=[binsX,binsX], cmin=1,cmap = colMap)
            self.plt.colorbar(img, ax=ax3)
            ax3.set( xlabel='pred %s'%unitStr, ylabel='pred-true %s'%unitStr)
            ax3.axhline(0, color='green', linestyle='--')
            ax3.set_xlim(mm1,mm2) ; ax3.set_ylim(-mm3,mm3)

  
#............................
    def fparams(self,U,tit,figId=6,tit2='', metaD=None):
        #print('ewew',U.shape)
        if metaD==None:  metaD=self.metaD
        parName=metaD['parName']
        nPar=metaD['numPar']
        
        nrow,ncol=2,3
        figId=self.smart_append(figId)
        fig=self.plt.figure(figId,facecolor='white', figsize=(2.7*ncol,2.2*nrow))
        unitStr=' (a.u.)'
        mm2=mm=self.maxU
        mm1=-mm2
        binsX= np.linspace(mm1,mm2,30)

        j=1
        for i in range(0,nPar,2):
            iPar=min(i,U.shape[1]-2) # account for odd num of params
            y1=U[:,iPar]
            y2=U[:,iPar+1]
            ax=self.plt.subplot(nrow,ncol,j)
            j+=1
            zsum,xbins,ybins,img = ax.hist2d(y1,y2,bins=binsX, cmin=1,
                                   cmap = cmap.rainbow)
            self.plt.colorbar(img, ax=ax)
            ax.set(title=tit+unitStr, xlabel='%d: %s'%(iPar,parName[iPar]), ylabel='%d: %s'%(iPar+1,parName[iPar+1]))
            ax.grid()
            if i==0: ax.text(0.1,0.85,tit2,transform=ax.transAxes)
            if self.maxU >1.3:
                rect1 = self.plt.Rectangle((-1,-1),2,2,fill=False,linestyle='--',edgecolor='r')
                ax.add_patch(rect1)

        for iPar in range(0,nPar):
            z=U[:,iPar]
            ax=self.plt.subplot(nrow,ncol,j) ; j+=1
            binsY=np.linspace(mm1,mm2,50)
            ax.hist(z,binsY)
            ax.set(xlabel='%d: %s%s'%(iPar,parName[iPar],unitStr),ylabel='frames')
            if iPar==0:  ax.set_title('n=%d'%z.shape[0])
            ax.grid()

            
#............................
    def plot_data_cubes(self,X,Y,msidx=0,idx=-1,tit='',figId=7):
        assert len(X) > idx # do not ask for a not existing frame

        frameDim=X[0].ndim
        print('xx',X.shape,' multi-sacle index=',msidx,frameDim)
        if idx<0:
            mxFr=X.shape[0]
            idx=np.random.randint(mxFr)

        if frameDim==5: 
            nrow,ncol=2,2
            figSize=(10,9)
            cube4D=X[0,msidx]
            nTime=cube4D.shape[3]  
            sh1=tuple(cube4D.shape[-4:-1])          
            cubeV=np.split(cube4D,nTime,axis=-1)
            for iT in range(nTime):
                print('res it=',iT,cubeV[iT].shape,sh1)
                cubeV[iT]=cubeV[iT].reshape(sh1)
        else:
            error12

        print('plot input for trace idx=',idx,' frameDim=',frameDim, 'nTime=',nTime)
    
        fig=self.plt.figure(figId,facecolor='white', figsize=figSize)        
        figId=self.smart_append(figId)

        angle=120
        wmax=np.max(cubeV[0])
        wthr=0.05*wmax
        print('will apply wthr=',wthr,cubeV[0].shape)
        for iT in range(nTime):
            ax = self.plt.subplot(nrow, ncol, iT+1, projection='3d')  
            cube=cubeV[iT]
            wmin=np.min(cube)
            wmax=np.max(cube)
            wsum=np.sum(cube)
            print('cube wmin=',wmin, ' wmax=',wmax, 'w sum=',wsum,cube.shape) 
 
            xs=[]; ys=[];zs=[]
            for i0 in range(cube.shape[0]):
                for i1 in range(cube.shape[1]):
                    for i2 in range(cube.shape[2]):
                        if cube[i0,i1,i2] <wthr: continue
                        xs.append(i0)
                        ys.append(i1)
                        zs.append(i2)
            print('wthr=',wthr,' nbin=',len(xs),'iT=',iT)
            ax.scatter(xs, ys, zs,alpha=0.5, s=0.4)

            ax.view_init(30, angle)

            if iT==0:
                tit=tit
                ptxtL=[ '%.2f'%x for x in Y[idx]]
                tit+=' U:'+', '.join(ptxtL)
                tit+=', thr=%d, msidx=%d'%(wthr,msidx)
            else:
                tit='time-bin=%d'%iT

            xtit='idx%d'%(idx) 
            ytit='sum=%.2e, max=%.2e'%(wmax,wsum)            
            ax.set(title=tit[:65], xlabel=xtit, ylabel=ytit)
