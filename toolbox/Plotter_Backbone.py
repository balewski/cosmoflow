import os
import numpy as np
#from matplotlib import cm as cmap
#from matplotlib.ticker import MaxNLocator


import socket  # for hostname
import time

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

#............................
#............................
#............................
class Plotter_Backbone(object):
    def __init__(self, args):
        import matplotlib as mpl
        if args.noXterm:
            mpl.use('Agg')  # to plot w/o X-server
            print(self.__class__.__name__,':','Graphics disabled')
        else:
            mpl.use('TkAgg') # on baci-desktop
            print(self.__class__.__name__,':','Graphics started')
        import matplotlib.pyplot as plt
        plt.close('all')
        self.plt=plt
        self.figL=[]
        self.outPath=args.outPath
        if self.outPath[-1]!='/' : self.outPath+='/'
        self.prjName=args.prjName

    #............................
    def display_all(self, ext='', png=1):
        if len(self.figL)<=0:
            print('display_all - nothing to plot, quit')
            return
        for fid in self.figL:
            self.plt.figure(fid)
            self.plt.tight_layout()
            #self.plt.subplots_adjust(bottom=0.06, top=0.97) # for 21-pred

            figName=self.outPath+'%s_%s_f%d'%(self.prjName,ext,fid)
            if png: figName+='.png'
            else: figName+='.pdf'
            print('Graphics saving to ',figName)
            self.plt.savefig(figName)
        self.plt.show()

# figId=self.smart_append(figId)
#...!...!....................
    def smart_append(self,id): # increment id if re-used
        while id in self.figL: id+=1
        self.figL.append(id)
        return id

