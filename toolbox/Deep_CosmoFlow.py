import os, time
import copy
import warnings
import socket  # for hostname
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings

startT0 = time.time()
import sys
sys.path.append(os.path.abspath("toolbox"))
from Util_IOfunc import  write_yaml, read_yaml
from Util_Func import MyPolynomialDecay, MyLearningTracker
from InpGen_CosmoFlow import Cosmo7_input_generator
from Util_Func import build_model_cosmo7

from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint , ReduceLROnPlateau,  LearningRateScheduler

import horovod.tensorflow.keras as hvd 

import tensorflow.keras.backend as K
import tensorflow as tf

import numpy as np
import h5py
import socket  # for hostname
VERBOSE_RANKS=[0]

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"


#............................
#............................
#............................
class Deep_CosmoFlow(object):
    #for multiple, independent "constructors" provide different class-methods. 

    def __init__(self,**kwargs):
        for k, v in kwargs.items():
            self.__setattr__(k, v)

        for xx in [  self.outPath]:
            if os.path.exists(xx): continue
            print('Aborting on start, missing  dir:',xx)
            exit(99)

        #print(self.__class__.__name__,'TF ver:', tf.__version__,', prj:',self.prjName)


    # alternative constructors
    @classmethod #........................
    def formater(cls, args):
        print('Cnst:form')
        obj=cls(**vars(args))
        obj.metaD={} # add empty 
        return obj

    @classmethod #........................
    def trainer(cls, args):
        obj=cls(**vars(args))
        # initialize Horovod - if requested
        if obj.useHorovod:
            obj.config_horovod()
            if obj.verb:
                for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
        else:
            obj.myRank=0
            obj.numRanks=1
            obj.hvd=None
            print('Horovod disabled')

        if obj.verb:
            print('Cnst:train, myRank=',obj.myRank)
            print('deep-libs imported TF ver:',tf.__version__,' elaT=%.1f sec,'%(time.time() - startT0))
            Lpd=tf.config.list_physical_devices('GPU')
            print('Lpd, devCnt=',len(Lpd), Lpd)


        obj.read_metaInp(obj.dataSplit)
        obj.train_hirD={'acc': [],'loss': [],'lr': [],'val_acc': [],'val_loss': []}
        obj.sumRec={}

        # . . . load hyperparameters
        hparF='./hpar_cosmo7_'+obj.modelDesign+'.yaml'
        bulk=read_yaml(hparF,verb=obj.verb)
        if obj.verb: print('bulk',bulk)
        obj.hparams=bulk

        # overwrite some hpar if provided from command line
        if obj.dropFrac!=None:  
            obj.hparams['dropFrac']=obj.dropFrac

        # add params missing in hpar on purpose, it must be defined by the runtime
        obj.hparams['train_conf']['localBS']=obj.localBS

        LRconf=obj.hparams['train_conf']['LRconf']
        LRconf['init']*=obj.initLRfactor
        if obj.verb: print('use initLR=%.3g'%LRconf['init'])
        
        # sanity checks                
        assert obj.hparams['fc_block']['dropFrac']>=0
        assert obj.hparams['train_conf']['localBS']>=1
        
        # refine location of input data
        if args.dataSource not in  obj.metaD['sourceDir']:
            inpDir=args.dataSource
            if obj.myRank==0: print('use user defined data path:',inpDir)
        else:  # use shortcut stored in yaml file
            BBname=obj.metaD['sourceDir'][args.dataSource]
            if 'bb' in  args.dataSource:
                inpDir = os.environ[BBname]
            else:
                inpDir=BBname
            #print('ee',inpDir,obj.metaD['subDir'])
            inpDir+=obj.metaD['subDir']
        obj.dataPath=inpDir
 
        return obj

    @classmethod #........................
    def predictor(cls, args):
        print('Cnst:pred')
        obj=cls(**vars(args))
        obj.read_metaInp(obj.dataSplit)  # . . .  load metaD

        # location of input data
        BBname=obj.metaD['sourceDir'][args.dataSource]
        if 'bb' in  args.dataSource:
            inpDir = os.environ[BBname]
        else:
            inpDir=BBname
            inpDir+=obj.metaD['subDir']
        obj.dataPath=inpDir
 
        return obj

#...!...!..................
    def config_horovod(self):
        import horovod.tensorflow.keras as hvd

        hvd.init()
        self.numRanks= hvd.size()
        self.myRank= hvd.rank()
        self.hvd=hvd
        self.verb*=self.myRank in VERBOSE_RANKS  # make silent other ranks
        #print('ggg',self.verb,self.myRank in VERBOSE_RANKS,VERBOSE_RANKS)

        print(' Horovod: initialize  myRank=%d of %d on %s verb=%d'%(self.myRank,self.numRanks,socket.gethostname(),self.verb))
        # Horovod: pin GPU to be used to process local rank (one GPU per process)
        # TF2
        assert tf.__version__[0]=='2'
        gpus = tf.config.experimental.list_physical_devices('GPU')

        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        if gpus:
            tf.config.experimental.set_visible_devices(gpus[hvd.local_rank()], 'GPU')

#............................
    def read_metaInp(self,metaF):
        self.metaD=read_yaml(metaF, verb=self.verb)
        if self.verb: print('read metaInp:',metaF,len(self.metaD),self.verb)
        # update prjName if not 'format'
        self.prjName+='_'+self.modelDesign
        

#............................
    def init_genetors(self):
        hpar=self.hparams

        if self.verb: print('init Generators, inpDir=%s, localBS=%d, localSamples='%(self.dataPath, hpar['train_conf']['localBS']),self.localSamples)

        # prep common config for train+val InpGen's
        genConf={'dataPath':self.dataPath,
                 'x_y_aux':False,'myRank':self.myRank,
                 'numLocalSamples':self.localSamples,'shuffle':self.shuffle_data
        }
        for x in ['localBS']:  genConf[x]=hpar['train_conf'][x]
        
        for x in ['h5nameTemplate','sampleShape','sampleNorm','numPar']:
            genConf[x]=self.metaD[x]

        self.sumRec['inpGen']={}
        self.inpGenD={}
        for dom in ['train','val']:
            if dom=='val' :
                genConf['numLocalSamples']=max(1, genConf['numLocalSamples']//8)
            genConf['fileIdxList']= copy.deepcopy(self.metaD['splitIdx'][dom])
            genConf['name']='IG-'+dom
            genConf['domain']=dom
            
            self.inpGenD[dom]=Cosmo7_input_generator(genConf,verb=self.myRank==0)
            self.sumRec['inpGen'][dom]=copy.deepcopy(genConf)
        #?self.sumRec['state']='model_build'
        self.sumRec['XY_shapes']=self.inpGenD[dom].XY_shapes()
        #print('ff',self.sumRec['XY_shapes'])


    #............................
    def build_model(self):
        hpar=self.hparams
        if self.verb: print('build_model hpar:',hpar)
        self.model,totParCnt=build_model_cosmo7(hpar,self.hvd,self.verb)
        # create summary record
        rec={'hyperParams':hpar,
             'modelWeightCnt':totParCnt,
             'modelLayerCnt':len(self.model.layers),
             'modelDesign' : self.modelDesign,
             'hostName' : socket.gethostname(),
             'numRanks': self.numRanks,
             'state': 'model_build',
             'jobId': self.jobId
        }
        self.sumRec.update(rec)

            
  #............................
    def train_model(self):
        hpar=self.hparams
        callbacks_list = []

        if self.useHorovod:
            # Horovod: broadcast initial variable states from rank 0 to all other processes.
            # This is necessary to ensure consistent initialization of all workers when
            # training is started with random weights or restored from a checkpoint.
            callbacks_list.append(hvd.callbacks.BroadcastGlobalVariablesCallback(0))
            # Note: This callback must be in the list before the ReduceLROnPlateau,
            # TensorBoard or other metrics-based callbacks.
            callbacks_list.append(hvd.callbacks.MetricAverageCallback())
            #Scale the learning rate `lr = 1.0` ---> `lr = 1.0 * hvd.size()` before
            # the first five epochs. See https://arxiv.org/abs/1706.02677 for details.i

            mutliAgent=hpar['train_conf']['multiAgent']
            if mutliAgent['warmup_epochs']>0 :
                callbacks_list.append(hvd.callbacks.LearningRateWarmupCallback(warmup_epochs=mutliAgent['warmup_epochs'], verbose=self.verb))
                if self.verb: print('added LearningRateWarmupCallback(%d epochs)'%mutliAgent['warmup_epochs'])


        lrCb=MyLearningTracker()
        callbacks_list.append(lrCb)

        if self.checkPtOn and self.myRank==0:
            outF5w=self.outPath+'/'+self.prjName+'.weights_best.h5'
            chkPer=1
            ckpt=ModelCheckpoint(outF5w, monitor='val_loss', save_best_only=True, save_weights_only=True, verbose=self.verb,period=chkPer)
            callbacks_list.append(ckpt)
            if self.verb: print('enabled ModelCheckpoint, period=',chkPer)
    
        LRconf=self.hparams['train_conf']['LRconf']
        if LRconf['patience']>0:
            [pati,fact]=LRconf['patience'],LRconf['reduceFactor']
            redu_lr = ReduceLROnPlateau(monitor='val_loss', factor=fact, patience=pati, min_lr=0.0, verbose=self.verb,min_delta=LRconf['min_delta'])
            callbacks_list.append(redu_lr)
            if self.verb: print('enabled ReduceLROnPlateau, patience=%d, factor =%.2f'%(pati,fact))

        ''' OFF for comso7   
        if self.hparams['LR_polynomialDecay'][0]>0:
            [maxEp,endLR,decPow]=self.hparams['LR_polynomialDecay']
            [optName, initLR]=hpar['optimizer']
            polDec_lr = MyPolynomialDecay(maxEpochs=maxEp, initLR=initLR, endLR=endLR, power=decPow,verb=0)
            callbacks_list.append(LearningRateScheduler(polDec_lr))
            if self.verb: print('enabled PolynomialDecay maxEpochs=%d, initLR=%.3g, endLR=%.3g, power=%.2f'%(maxEp,initLR,endLR,decPow))
        '''
        if self.earlyStopPatience>0:
            earlyStop=EarlyStopping(monitor='val_loss', patience=self.earlyStopPatience, verbose=self.verb, min_delta=LRconf['min_delta'])
            callbacks_list.append(earlyStop)
            if self.verb: print('enabled EarlyStopping, patience=',self.earlyStopPatience)

        fitVerb=1   # prints live:  [=========>....] - ETA: xx s 
        if self.verb==2: fitVerb=1
        if self.verb==0: fitVerb=2  # prints 1-line summary at epoch end

        if self.numRanks>1:  # change the logic
            if self.verb :
                fitVerb=2
            else:
              fitVerb=0 # keras is silent  

        startTm = time.time()
        hir=self.model.fit(  # TF-2.1
            self.inpGenD['train'],callbacks=callbacks_list,
            epochs=self.goalEpochs, max_queue_size=10,
            workers=1, use_multiprocessing=False,
            shuffle=self.shuffle_data, verbose=fitVerb,
            validation_data=self.inpGenD['val']
        )
        fitTime=time.time() - startTm
        hir=hir.history
        totEpochs=len(hir['loss'])
        earlyStopOccured=totEpochs<self.goalEpochs

        for obs in hir:
            rec=[ float(x) for x in hir[obs] ]
            self.train_hirD[obs].extend(rec)

        # this is a hack, 'lr' is returned by fit only when --reduceLr is used
        if 'lr' not in  hir:
            self.train_hirD['lr'].extend(lrCb.hir)

        self.train_hirD['steps_per_epoch']=self.inpGenD['train'].__len__()
        self.train_hirD['stepTimeHist']=self.inpGenD['train'].stepTime
        #if self.train_hirD['stepTimeHist'][1]>len(self.train_hirD['stepTimeHist'][1]):
        #    self.train_hirD['stepTimeHist']=self.train_hirD['stepTimeHist'][1:]
            # not sure why this list hs 1 more element w/ Horovod

        #report performance for the last epoch
        lossT=self.train_hirD['loss'][-1]
        lossV=self.train_hirD['val_loss'][-1]
        lossVbest=min(self.train_hirD['val_loss'])

        self.train_sec=fitTime
        # add info to summary
        rec=self.sumRec
        rec['earlyStopOccured']=int(earlyStopOccured)
        rec['fitMin']=fitTime/60.
        rec['totEpochs']=totEpochs
        rec['train_loss']=float(lossT)
        rec['val_loss']=float(lossV)
        rec['steps_per_epoch']=self.inpGenD['train'].__len__()
        rec['state']='model_trained'
        rec['rank']=self.myRank
        rec['num_ranks']=self.numRanks
        rec['num_open_files']=len(self.inpGenD['train'].conf['fileIdxList'])

        if self.verb: 
            print('\n End Val Loss=%s:%.3g, best:%.3g'%(hpar['train_conf']['lossName'],lossV,lossVbest),', %d totEpochs, fit=%.1f min , numRanks=%d,'%(totEpochs,fitTime/60.,self.numRanks),rec['jobId'],' hpar:',hpar)
        
    #............................
    def save_model_full(self):
        model=self.model
        fname=self.outPath+'/'+self.prjName+'.model.h5'     
        print('save model  to',fname)
        model.save(fname)
        return fname


   #............................
    def load_model_full(self,path='',verb=1):
        fname=path+'/'+self.prjName+'.model.h5'     
        print('load model from ',fname)
        self.model=load_model(fname) # creates model from existing HDF5
        if verb:  self.model.summary()


    #............................
    def load_weights_only(self,path='.'):
        start = time.time()
        inpF5m=path+'/'+self.prjName+'.weights_best.h5'  #_best
        if self.verb:
            print('load  weights  from',inpF5m,end='... ')
        self.model.load_weights(inpF5m) # restores weights from HDF5
        if self.verb:
            print('loaded, elaT=%.2f sec'%(time.time() - start))

 
