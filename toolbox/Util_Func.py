import numpy as np
from scipy import signal # for gauss function
from scipy.stats import stats # for Pearson correlation
import time,  os

import h5py
import random
from tensorflow.keras.callbacks import Callback
import tensorflow.keras.backend as K

from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Dropout,   Input, Conv3D,MaxPool3D,Flatten,Reshape, Lambda, BatchNormalization,AveragePooling3D
from tensorflow.keras.optimizers import Adadelta
import tensorflow as tf


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class MyLearningTracker(Callback):
    def __init__(self):
        self.hir=[]
    def on_epoch_end(self, epoch, logs={}):
        optimizer = self.model.optimizer
        lr = K.eval(optimizer.lr)
        self.hir.append(float(lr))


# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
class MyPolynomialDecay():
    def __init__(self, maxEpochs=100, initLR=0.01, endLR=1.e-7, power=1.0, verb=1):
        # store the maximum number of epochs, base learning rate,
        # and power of the polynomial
        self.maxEpochs = maxEpochs
        self.initAlpha = initLR
        self.endLR = endLR
        self.power = power
        if verb: print('\nMyPolynomialDecay activated, maxEpochs=%d, initLR=%.3g, endLR=%.3g, power=%.2f'%(maxEpochs,initLR,endLR,power))
    def __call__(self, epoch):
        # compute the new learning rate based on polynomial decay
        decay = (1 - (epoch / float(self.maxEpochs))) ** self.power
        alpha = self.initAlpha * decay
        alpha=max(self.endLR, alpha) # protect against negative decay
        return float(alpha) # return the new learning rate


#...!...!....................
def get_correlation(aV,bV):
        n=len(aV)
        assert n >2
        assert n == len(bV)
        rho,p_val=stats.pearsonr(aV,bV)
        #print(n,rho,p_val)
        # based on  https://stats.stackexchange.com/questions/226380/derivation-of-the-standard-error-for-pearsons-correlation-coefficient

        sigRho=np.sqrt( (1-rho*rho)/(n-2))
        #print('rho=',rho,'sigRho=',sigRho)
        return rho,sigRho

        
#...!...!..................
def get_UmZ_correl(UmZ,dom,parNameL):
        #print('a dims:',a.shape,'\n dump a:')

        # compute cov matrix
        c=np.cov(UmZ,rowvar=False)
        nv=c.shape[0]
        nvL=range(nv)
        #print('cov M:')
        #pprint(c)
        c1=np.diagonal(c)
        rmseU=np.sqrt(c1)
        print('one RMSE for UmZ:',rmseU)
       
        avrSig=rmseU.mean()
        print('global RMS avr=%.3f , var=%.3f '%(avrSig, avrSig*avrSig))
        
        print('\nnames ',end='')
        [ print('   %2d:%s'%(i,parNameL[i]),end='') for i in nvL]

        print('\nresidue:',end='')
        [ print(' U%d-> %4.1f%s  '%(i,100*rmseU[i],chr(37)),end='') for i in nvL ]

        lossMAE=np.absolute(UmZ).mean()

        lossMSE=c1.mean()
        print('\n global lossMSE=%.4f , lossMAE=%.3f (computed), dom=%s, nEve=%d'%( lossMSE,lossMAE,dom,UmZ.shape[0]))
 
        #print('\ncorrelation matrix for %d dim vector'%nv)
        r=np.corrcoef(UmZ,rowvar=False)
        #pprint(r)

        print('\nnames  ',end='')
        [ print(' %2d:%s'%(i,parNameL[i]),end='') for i in nvL]
        print('\ncorrM  ',end='')
        [ print('  __var%1d__ '%i,end='') for i in nvL]
        for k in nvL:
            print('\n var%1d '%k,end='')
            [ print('     %6.3f'% r[i][k],end='') for i in range(k+1)]
            #print('      #  '*(nv-k),end='')
        print()
        return lossMSE


#...!...!....................
def locateHD5data(mainSrc, subDir,nameMask,verb=1):

    source_dir = mainSrc+"/"+subDir+"/"
    for xx in [ mainSrc,  source_dir]:
        if os.path.exists(xx): continue
        print('Aborting on start, missing  dir:',xx)
        exit(1)

    if verb>0: print('use source_dir:',source_dir)
    allL=os.listdir(source_dir)

    outL=[]
    for x in allL:
        if 'hdf5' not in x: continue
        if nameMask not in x: continue
        #fullN=source_dir+'/'+x
        fullN=x
        if len(outL)<1 and verb>0 :
            print('example full name:',fullN)
        outL.append(fullN)
    if verb>0:  print('found %d HD5 for mask=%s'%(len(outL),nameMask))
    return source_dir,sorted(outL)


#...!...!....................
def rebin3d(cube):
    assert cube.ndim==4
    # cube must be symmetric
    assert cube.shape[0]==cube.shape[1]
    assert cube.shape[2]==cube.shape[1]
    nb=2 # rebin factor
    a=cube.shape[0]
    assert a%nb==0
    b=a//nb
    nz=cube.shape[3]
    #print('aaa',a,nb,b)
    sh = b,nb,b,nb,b,nb,nz
    C=cube.reshape(sh)
    #print('Csh',C.shape)
    D=np.sum(C,axis=-2)
    #print('Dsh',D.shape)
    E=np.sum(D,axis=-3)
    #print('Esh',E.shape)
    F=np.sum(E,axis=-4)
    #print('Fsh',F.shape)
    return F


#...!...!....................
def multiScale_cube512x4(inpAr):
    cube4=inpAr.astype(np.uint32) # to not loose spikes after hadd
    base=128 # target dimension of output cube
    print('  MultiScale: inp ',cube4.shape,inpAr.dtype)
    assert cube4.ndim==4
        
    ndim=3
    boxL=[]
    for m in range(2):  # will rebin twice
        inDim=cube4.shape[0]
        mxOff=inDim-base
        x=np.random.choice(mxOff, ndim)
        #print(m,'=m, offV',x,'inDim:',inDim,'mxOff:',mxOff)
        out=cube4[x[0]:x[0]+base,x[1]:x[1]+base,x[2]:x[2]+base ].copy()
        #print('out',out.shape)
        boxL.append(out)
        cube4=rebin3d(cube4)

    # add final cube to the list
    boxL.append(cube4)

    # clamp and down-cast 128^3 cubes
    stackL=[]
    for cube in boxL:
        A=np.clip(cube,0,65000)
        B=A.astype(np.uint16)
        stackL.append(B)

    # .concatenate all 128^3 cubes in to giant array
    C=np.concatenate(stackL,axis=-1) # does (?,128,128,128,12)
    #C=np.stack(stackL,axis=0) # does (?,3,128,128,128,4)
    print('  endJJ',C.shape,C.dtype)#, 'sum=',np.sum(C),' max/min=',np.max(C),np.min(C))
    return C

'''  # old testing code
        for i in range(nz):
            cube=cube4[...,i]
            print(i,'cube sh',cube.shape)                
            j=40; k=j//2
            s1=cube[j:j+2,j:j+2,j:j+2]
            print(j,'s1',s1, np.sum(s1))
            print(k,'H',cubeHalf[k,k,k])
            exit(1)
'''
#...!...!....................
def oneScale_cube512x4(inpAr):
    cube4=inpAr
    base=128 # target dimension of output cube
    print('  OneScale: inp ',cube4.shape,inpAr.dtype)
    assert cube4.ndim==4
        
    n=3
    inDim=cube4.shape[0]
    mxOff=inDim-base
    x=np.random.choice(mxOff, n)
    C=cube4[x[0]:x[0]+base,x[1]:x[1]+base,x[2]:x[2]+base ].copy()
    print('  endJJ',C.shape,C.dtype)#, 'sum=',np.sum(C),' max/min=',np.max(C),np.min(C))
    return C

#............................ 
def split_onto_domains(allL):
        frac=0.10
        nTot=len(allL)
        nFrac=int(nTot*frac)

        assert nFrac>=2

        print('allL',allL[:10], len(allL))
        # shuffle list in place
        random.shuffle(allL)
        #print('shaffL',allL[:10])

        outD={}
        outD['test']=allL[:nFrac]
        outD['val']=allL[nFrac:2*nFrac]
        outD['train']=allL[2*nFrac:]

        for dom in outD:
            print(dom, ' size',len(outD[dom]))

        return outD


#...!...!....................
def build_model_cosmo7(hpar,hvd=None,verb=1):
    inpLyr=Input(shape=tuple(hpar['inpShape']),name='inp')
    lyr=inpLyr

    # assembly CNN block
    for block in ['A','B']:
        if block=='B' and hpar['batch_norm_cnn']:  lyr=BatchNormalization()(lyr)
        C=hpar['cnn_block_'+block]
        for dim in C['filters']:           
            lyr=Conv3D(dim,C['kernel'],strides=C['strides'],activation=C['activation'],padding=C['padding'] )(lyr)
            if C['pool_type']=='avr':
                lyr=AveragePooling3D(pool_size=C['pool_size'],padding=C['padding'])(lyr)
            elif C['pool_type']=='max':
                lyr=MaxPool3D(pool_size=C['pool_size'],padding=C['padding'])(lyr)
            else:
                kill_56
  
    # transition to FC
    lyr=Flatten(name='to_1d')(lyr)

    if hpar['batch_norm_flat']:  lyr=BatchNormalization()(lyr)
    
    # assembly FC block
    C=hpar['fc_block']
    fc_act=C['activation']
    dropFrac=C['dropFrac']
    for fc_dim in C['units']:   
        lyr=Dense(fc_dim,activation=fc_act)(lyr)
        if dropFrac>0:
            lyr= Dropout(dropFrac)(lyr)

    # add last layer with proper activation
    outAmpl=1.2
    lyr=Dense(hpar['outShape'], activation='tanh')(lyr)
    lyr=Lambda(lambda val: val*outAmpl)(lyr)

    model = Model(inputs=inpLyr, outputs=lyr)
    totLyrCnt=len(model.layers)
    totParCnt=model.count_params()

    C=hpar['train_conf']
    lossName=C['lossName']
    optName=C['optName']
    initLR=C['LRconf']['init']

    if verb:
        model.summary() # will print 
        print('compile model',optName,lossName)

    if optName=='adam' :
        opt=tf.keras.optimizers.Adam(lr=initLR)
    elif optName=='nadam' :
        opt=tf.keras.optimizers.Nadam(lr=initLR)
    elif optName=='adadelta' :
        opt = tf.keras.optimizers.Adadelta(lr=initLR)
    elif optName=='adagrad' :
        opt = tf.keras.optimizers.Adagrad(lr=initLR)
    else:
        crash_21

    lossName=hpar['train_conf']['lossName']
    # add Horovod if requested
    if hvd!=None:
        if verb: print('  add Horovod Distributed Optimizer, initLR=',initLR)
        opt = hvd.DistributedOptimizer(opt)
        model.compile(optimizer=opt, loss=lossName,experimental_run_tf_function=False) # needed for TF2
    else:  # noHorovod 1-worker training 
        model.compile(optimizer=opt, loss=lossName)
    
    if verb:
        print('Model summary   layers=%d , params=%.1f K, inputs:'%(totLyrCnt,totParCnt/1000.),model.input_shape,'output:',model.output_shape)

    return model,totParCnt


    
