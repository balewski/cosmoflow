#!/usr/bin/env python
""" 
toy testing Horovod w/ TF2
taken from
https://horovod.readthedocs.io/en/stable/keras.html

modiffed by Jan Balewski
on Cori do: 
 module load tensorflow/gpu-2.1.0-py37

 srun -n1 ./train_Mnist_Horovod_TF2.py

Use Shifter image for DGX
salloc -C dgx -N 1 -t 4:00:00 -c 10 --gres=gpu:2 --ntasks-per-node=2  -w cgpu19 

Note: this is depreciated:  --gres=gpu:2
This is advised:  --gpus-per-socket and --gpu-bind=closest 


srun -n1  shifter --image nersc/tensorflow:ngc-20.08-tf2-v1  ./train_Mnist_Horovod_TF2.py
echo inShifter:`env|grep  SHIFTER_RUNTIME`


 shifter --image nersc/tensorflow:ngc-20.09-tf2-v0  bash
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"
# Copyright 2019 Uber Technologies, Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import time
import socket  # for hostname

import tensorflow as tf
Lpd=tf.config.list_physical_devices('GPU')
print('\n\nM:Lpd, devCnt=',len(Lpd), Lpd)

import horovod.tensorflow.keras as hvd

# Horovod: initialize Horovod.
hvd.init()

print('\nM: horovod initialized')

# Horovod: pin GPU to be used to process local rank (one GPU per process)
gpus = tf.config.experimental.list_physical_devices('GPU')

numRanks= hvd.size()
myRank= hvd.rank()
locRank=hvd.local_rank()
locGPUs=len(gpus)

print(' Horovod: initialize  myRank=%d of %d on %s locGPUs=%d , locRank=%d'%(myRank,numRanks,socket.gethostname(),locGPUs,locRank))

for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

if gpus:
    print('M:rank=%d, locRank=%d  host=%s'%(hvd.rank(),hvd.local_rank(),socket.gethostname()))
    print('M:rank=%d, see GPUs'%(hvd.rank()),gpus[hvd.local_rank()])
    tf.config.experimental.set_visible_devices(gpus[hvd.local_rank()], 'GPU')
    
print('M:step1')

(mnist_images, mnist_labels), _ = \
    tf.keras.datasets.mnist.load_data(path='mnist-%d.npz' % hvd.rank())

print('M:step2')

dataset = tf.data.Dataset.from_tensor_slices(
    (tf.cast(mnist_images[..., tf.newaxis] / 255.0, tf.float32),
             tf.cast(mnist_labels, tf.int64))
)

dataset = dataset.repeat().shuffle(10000).batch(256)


mnist_model = tf.keras.Sequential([
    tf.keras.layers.Conv2D(32, [3, 3], activation='relu'),
    tf.keras.layers.Conv2D(64, [3, 3], activation='relu'),
    tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
    tf.keras.layers.Dropout(0.25),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(10, activation='softmax')
])

# Horovod: adjust learning rate based on number of GPUs.
scaled_lr = 0.001 * hvd.size()
opt = tf.optimizers.Adam(scaled_lr)

# Horovod: add Horovod DistributedOptimizer.
opt = hvd.DistributedOptimizer(opt)  # Works only w/ TF2.1
# see : https://github.com/horovod/horovod/issues/2001
#opt=hvd.DistributedGradientTape(opt)  # <<== CRASH HERE in TF2.1

# Horovod: Specify `experimental_run_tf_function=False` to ensure TensorFlow
# uses hvd.DistributedOptimizer() to compute gradients.
mnist_model.compile(loss=tf.losses.SparseCategoricalCrossentropy(),
                    optimizer=opt,
                    metrics=['accuracy'],
                    experimental_run_tf_function=False)

callbacks = [
    # Horovod: broadcast initial variable states from rank 0 to all other processes.
    # This is necessary to ensure consistent initialization of all workers when
    # training is started with random weights or restored from a checkpoint.
    hvd.callbacks.BroadcastGlobalVariablesCallback(0),

    # Horovod: average metrics among workers at the end of every epoch.
    #
    # Note: This callback must be in the list before the ReduceLROnPlateau,
    # TensorBoard or other metrics-based callbacks.
    hvd.callbacks.MetricAverageCallback(),

    # Horovod: using `lr = 1.0 * hvd.size()` from the very beginning leads to worse final
    # accuracy. Scale the learning rate `lr = 1.0` ---> `lr = 1.0 * hvd.size()` during
    # the first three epochs. See https://arxiv.org/abs/1706.02677 for details.
    # skip , initial_lr=scaled_lr
    hvd.callbacks.LearningRateWarmupCallback(warmup_epochs=3 , verbose=1),
]

# Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
if hvd.rank() == 0:
    callbacks.append(tf.keras.callbacks.ModelCheckpoint('./checkpoint-{epoch}.h5'))

print('M: my rank is %d of %d'%(hvd.rank(),hvd.size()))
# Horovod: write logs on worker 0.
verbose = 1 if hvd.rank() == 0 else 0

# Train the model.
# Horovod: adjust number of steps based on number of GPUs.
startT = time.time()
mnist_model.fit(dataset, steps_per_epoch=512 // hvd.size(), callbacks=callbacks, epochs=12, verbose=verbose)
if verbose==0: exit(0)

print('M:train time %d sec on %d ranks'%((time.time()-startT),hvd.size()))
score = mnist_model.evaluate(dataset, steps=100,verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

'''
 speed on Cori, 2020-08-21  TF 2.1
-n1 : 3s/ep, 6ms/step, 45.5 sec/12 ep
-n2 : 3s/ep, 10ms/step, 39.5 sec/12 ep

'''
