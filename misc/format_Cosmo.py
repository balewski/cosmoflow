#!/usr/bin/env python
""" 
format input for  CosmoFlow3.1, works on interactive haswell node
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Util_Func import locateHD5data,read_data_hdf5,write_data_hdf5, read_yaml , write_yaml, multiScale_cube512x4, oneScale_cube512x4,split_onto_domains

from pprint import pprint
import numpy as np

import argparse, os 
def get_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")
    parser.add_argument("-j", "--jobIdx", type=int, default=0,
                        help="job dir index")

    args = parser.parse_args()
    args.prjName='cosmoFlow3'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


#...!...!....................
def pack_and_write(cubeL,unitL,physL,nPack,metaD):
    print('\nPack_and_write rec=',nPack)

    #outPath=metaD['sourceDir']['csc']+metaD['subDir']
    outPath=metaD['sourceDir']['csc']+'/tryG/'
    outF='PeterA_'+metaD['coreName']+'-rec%d.h5'%nPack
    #print('nnn',outPath, outF)
    # stack all arrays
    outD={}
    outD['3Dmap']= np.stack(cubeL,axis=0)
    outD['unitPar']=np.stack(unitL,axis=0)
    outD['physPar']=np.stack(physL,axis=0)
    #print('3Dmap',outD['3Dmap'].shape,outD['3Dmap'].dtype)
    #print('in', cubeL[0].dtype)
    fullOutF=outPath+outF
    write_data_hdf5(outD,fullOutF)
    metaD['recId'].append(nPack)
    return nPack+1,outPath+outF


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

'''  # only generate shuffled indexes
allL=[]
for j0 in range(10):
    i0=20+10*j0; i1=i0+8
    for i in range(i0,i1):
       allL.append(i)    
print(allL)
outD=split_onto_domains(allL)
print(outD)
ok555
#'''

coreName="2019_05_4parE"

#.... input:
#inpSrc="/global/cscratch1/sd/balewski/cosmoUniverse_%s/"%coreName
inpSrc="/gpfs/alpine/ast153/scratch/balewski/cosmoUniverse_"+coreName+"/dim512_nT4_h5/"  # on Summit
inpSrc="/project/projectdirs/m3363/www/cosmoUniverse_"+coreName+"/"  # on Cori
dimMask='ics_2019-03'

jobDirL=[22309462, 21688988,  21812950,  21922619,  21929749,  21997469,  22021490,  22059249,  22074825,	22098324,  22118427] # 1st has 19 files, the rest have 1k files each

frameCnt=128
subDir=str(jobDirL[args.jobIdx])
nPack=10+ args.jobIdx*10

#.... output:
#smtName="/gpfs/alpine/ast153/scratch/balewski/cosmoUniverse_"+coreName+'/' # on Summit
outPath="/global/cscratch1/sd/balewski/cosmoUniverse_"+coreName+'_peter/' # on Cori

sourceDir,allL=locateHD5data(inpSrc,subDir,dimMask)
print('M: got len=',len(allL),' sourceDir=',sourceDir)

oneN=allL[7]

print('test-read one:',oneN)
blob1=read_data_hdf5(sourceDir+oneN)
print('see keys:',blob1.keys(), 'unitPar dump:', blob1['unitPar'][:5])
for x in blob1:
    ar=blob1[x]
    if ar.ndim >1: continue
    print(x,ar)
    if x=='namePar' :
        parNameL=list(map(lambda x: x.decode('UTF-8'), ar))
        print(parNameL)


# - - - - - - - -
# accumulate universes and predidically save them

#---- init metaD
metaD={}
metaD['sourceDir']={'smt':None,'csc':outPath}
metaD['subDir']='dim512_nT4_h5/'
metaD['frameCnt']=frameCnt
metaD['coreName']=coreName
#metaF=metaD['sourceDir']['csc']+metaD['subDir']+'/meta.cosmo_PeterA_%d.yaml'%nPack
metaD['recId']=[]
metaF=metaD['sourceDir']['csc']+'/meta.cosmo_peter_%d.yaml'%nPack

# - - - - main loop over inpt HD5 - - - - 
nUniv=0
for oneN in allL:
    #if nPack>16: break
    if nUniv>=frameCnt:
        nPack,outF=pack_and_write(stackCube,stackUnit,stackPhys,nPack,metaD)
        nUniv=0
        
    if nUniv==0:                      
        stackCube=[]; stackPhys=[]; stackUnit=[]

    print(nPack,nUniv,'read one:',oneN)
    blob=read_data_hdf5(sourceDir+oneN,verb=nUniv==0)
    cube4x3=multiScale_cube512x4(blob['full'])
    #bcube4x3=oneScale_cube512x4(blob['full']) 
    #cube4x3=np.zeros((1,128,128,128,12),dtype=np.int16) # hack for speed-testing
    stackCube.append(cube4x3)
    stackUnit.append(blob['unitPar'])
    stackPhys.append(blob['physPar'])
    nUniv+=1
    #if nUniv >1 : break

if nUniv>=frameCnt//2:
    nPack,outF=pack_and_write(stackCube,stackUnit,stackPhys,nPack,metaD)
else:
    print('drop last %d frames'%nUniv)
metaD['parNames']=parNameL
metaD['numPar']=len(metaD['parNames'])

# h5-write : 3Dmap (50, 3, 128, 128, 128, 4)

write_yaml(metaD,metaF)
print('Jan-end')

print('\nOptional plot content of **One** HD5 file=',outF)
blob2=read_data_hdf5(outF)
from Plotter_CosmoFlow import Plotter_CosmoFlow
gra=Plotter_CosmoFlow(args,metaD)
gra.plot_data_fparams(blob2['unitPar'],tit='true Us', figId=14)
#gra.plot_data_cubes(blob2['3Dmap'],blob2['unitPar']) 
gra.display_all(args,'form')

# time ./format_Cosmo.py -X -j 1

