#!/usr/bin/env python
""" 
evaluate scaling for multi-node cosmoFlow
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Plotter_Backbone import Plotter_Backbone
import numpy as np
import  time

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')


    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
 
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")


    args = parser.parse_args()
    args.prjName='cosmo5'
    args.sourcePath='/global/cscratch1/sd/balewski/cosmo5b/'

    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args

#............................
#............................
#............................
class Plotter_Design(Plotter_Backbone):
    def __init__(self, args):
        Plotter_Backbone.__init__(self,args)

#............................
    def plot_scaling1(self,sumD,figId=21,title=''):
        nrow,ncol=1,1
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(6,5))

        ax=self.plt.subplot(nrow,ncol,1)

        nrV={8:[], 1:[]} # numRanks       
        frsV={8:[], 1:[]} # global frames per second for BS=8 or 1
        for jid in sumD:
            rec=sumD[jid]
            #print('rec=',rec)
            nr=rec['numRanks']
            wt=rec['fitMin']*60
            numEpoch=rec['usedEpochs']
            stepsPerEpoch=rec['steps_per_epoch']
            batchSize=rec['hyperParams']['batch_size']
            globFrmPerSec= batchSize * stepsPerEpoch * numEpoch*nr/wt
            print('qq', batchSize , stepsPerEpoch , numEpoch,nr,wt,globFrmPerSec)
            frsV[batchSize].append(globFrmPerSec)
            nrV[batchSize].append(nr)


        for bs in nrV:
            ax.plot(nrV[bs],frsV[bs],'o-',label='BS=%d'%bs) 
        #if j==0: ax.text(0.1,0.85,'n=%d'%len(vals),transform=ax.transAxes)
        
        if 'Summit' in title:
            ax.set_xscale('log')
            ax.set_yscale('log')
        else:
            ax.set_xlim(0,)
            ax.set_ylim(0,)

        ax.grid(True)
        ax.set( xlabel='num GPUs', ylabel='global frames/sec', title='CosmoFlow on '+title)

        bs=8
        dr=nrV[bs][-1] - nrV[bs][0]
        dfrsId=dr*frsV[bs][0]/nrV[bs][0] + frsV[bs][0]
        print('ideal frms=',dfrsId, bs)
        ax.plot([0, nrV[bs][-1]], [0,dfrsId], color='magenta', linestyle='--',linewidth=1, label='ideal') 

        ax.legend(loc='best')   
 
#............................
    def plot_endLoss(self,sumD,figId=22,title=''):
        nrow,ncol=1,1
        #  grid is (yN,xN) - y=0 is at the top,  so dumm
        self.figL.append(figId)
        self.plt.figure(figId,facecolor='white', figsize=(6,3))

        ax=self.plt.subplot(nrow,ncol,1)

        nrV={8:[], 1:[]} # numRanks
        elV={8:[], 1:[]} # end-loss
        wtV={8:[], 1:[]}  # fit wall  time (min)

        for jid in sumD:
            rec=sumD[jid]
            #print('rec=',rec)
            batchSize=rec['hyperParams']['batch_size']
            nr=rec['numRanks']
            nrV[batchSize].append(nr)
            wt=rec['fitMin']
            wtV[batchSize].append(wt)
            el=rec['val_loss']
            print('qq',el, nr,wt)
            elV[batchSize].append(el)
 
        print(wtV,elV)
        for bs in nrV:
            ax.scatter(wtV[bs],elV[bs],label='BS=%d'%bs)
            for x,y,g in zip(wtV[bs],elV[bs],nrV[bs]):
                if g==1: 
                    tt="%d GPU"%g
                else: 
                    tt="%d"%g
                ax.text(x-0.5,y+0.005,tt)

        #ax.set_yscale('log')
        ax.grid(True)
        ax.set( xlabel='wall time (min)', ylabel='end val loss', title='CosmoFlow on '+title)

        ax.set_xlim(0,)
        #ax.set_ylim(min(elV)*.9,)
        #ax.set_xlim(wtV[0]*.8,)

        ax.legend(loc='best')   


import os
from Util_Func import write_yaml, read_yaml
# - - - - - - - - - - - - - - - - - - - - - - 
# - - - - - - - - - - - - - - - - - - - - - - 
def locateSummaryData(src_dir,verb=1):

    for xx in [ src_dir]:
        if os.path.exists(xx): continue
        print('Aborting on start, missing  dir:',xx)
        exit(1)

    if verb>0: print('use src_dir:',src_dir)

    sumF=src_dir+'/out/cosmoFlow5_cnn1.sum.yaml'
    try:
        sumD=read_yaml(sumF)
    except:
        crashIt34
        
    if verb>1 :print(sumD)
    return sumD
            

#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

sumD={}

'''
slurmJidL=[181083, 181087, 181089, 181080]
for jid in slurmJidL:
    inpPath='%s/%d/7/'%(args.sourcePath, jid) 
    sumD1=locateSummaryData(inpPath)
    sumD[jid]=sumD1
'''
sumD[513949]={'numRanks':1, 'fitMin':38.5 , 'val_loss' :0.127  }
sumD[513950]={'numRanks':6, 'fitMin':47.1 , 'val_loss' : 0.115}
sumD[513951]={'numRanks':12, 'fitMin':48.0 , 'val_loss' :0.08 }
sumD[513952]={'numRanks':24, 'fitMin': 48.6, 'val_loss' :0.09 }
sumD[513953]={'numRanks':60, 'fitMin':49.9 , 'val_loss' :0.076 }
sumD[513954]={'numRanks':120, 'fitMin':57.6 , 'val_loss' :0.067 }
sumD[513955]={'numRanks':240, 'fitMin':59.2 , 'val_loss' : 0.106}
sumD[513995]={'numRanks':120, 'fitMin':37.7 , 'val_loss' :0.084 }
sumD[513996]={'numRanks':240, 'fitMin':43.6 , 'val_loss' :0.084 }
sumD[513997]={'numRanks':600, 'fitMin':53.6 , 'val_loss' :0.070 }
sumD[513998]={'numRanks':1200, 'fitMin':61.4 , 'val_loss' :0.055 }
sumD[513999]={'numRanks':2400, 'fitMin': 74.7, 'val_loss' :0.044 }
#sumD[51399]={'numRanks':, 'fitMin': , 'val_loss' : }
#sumD[51399]={'numRanks':, 'fitMin': , 'val_loss' : }
#sumD[51399]={'numRanks':, 'fitMin': , 'val_loss' : }

for id in sumD:
    sumD[id]['steps_per_epoch']=400
    if id < 513980:
        sumD[id]['usedEpochs']=20
        sumD[id]['hyperParams']={'batch_size':8}
    else:
        sumD[id]['usedEpochs']=80
        sumD[id]['hyperParams']={'batch_size':1}
        

print('M: got num sum=',len(sumD))
gra=Plotter_Design(args)

tit='Cori-GPUs'
tit='Summit-GPUs'
gra.plot_scaling1(sumD, title=tit)
gra.plot_endLoss(sumD, title=tit)
gra.display_all(args,'scaling')  
