#!/usr/bin/env python
""" 
 change format of dataset,  used for tryG
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

from Util_Func import locateHD5data,read_data_hdf5,write_data_hdf5, read_yaml , write_yaml

from pprint import pprint
import numpy as np

import argparse, os 
def get_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')
    parser.add_argument( "-X","--noXterm", dest='noXterm',
                         action='store_true', default=False,
                         help="disable X-term for batch mode")

    parser.add_argument("--outPath",
                        default='out',help="output path for plots and tables")

    args = parser.parse_args()
    args.prjName='cosmoFlow6'
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    return args


def repack_one(inpPath, outPath, fname):
    #print('\nRP: read:',inpPath, outPath, fname)
    blob=read_data_hdf5(inpPath+fname)
    A=blob['3Dmap']
    XL=[]
    for i in range(4):
        i1=i*32; i2=i1+32
        if A.shape[0] <i1+8: continue
        X=A[i1:i2]
        print(i,'Xi.shape=',X.shape,X.dtype)
        MUST SPlit alos unit & phys par
        XL.append(X)
    return blob, XL
   
#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()

metaF='cosmo6_peterG.yaml'
metaD=read_yaml(metaF)


inpDataPath=metaD['sourceDir']['csc']+metaD['subDir']
fnameTmpl=metaD['fnameTmpl']
outDataPath=metaD['sourceDir']['csc']+'/tryG_uint16_32fr/'

meta2={}
sum=0
for dom in metaD['splitIdx']:
    recL=metaD['splitIdx'][dom]
    meta2[dom]=[]
    sum+=len(recL)
    print('see dom=%s  num files=%d, sum=%d'%(dom,len(recL),sum))
    for ir in recL:
        fname=fnameTmpl.replace('*',str(ir))
        blob, XL=repack_one(inpDataPath, outDataPath, fname)
        for ab, B in zip(['a','b','c','d'],XL):
            key=str(ir)+ab
            meta2[dom].append(key)
            fname=fnameTmpl.replace('*',key)
            blob['3Dmap']=B
            write_data_hdf5(blob,outDataPath+fname)
        #break
    print('\n\n done w/ dom=',dom)
    #break
metaD['splitIdx']=meta2
write_yaml(metaD,'repack_metaD.yaml')
        
