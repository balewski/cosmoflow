#!/bin/bash
design=$1
events=$2

echo CosmoAtSummit design=$design epochs=$epochs  events=$events

# Disable multiple threads
export OMPI_MCA_osc_pami_allow_thread_multiple=0

#disable adaptive routing
export PAMI_IBV_ENABLE_OOO_AR=0
export PAMI_IBV_QP_SERVICE_LEVEL=0

# Reduce horovod sleep time, enable priority NCCL stream
export HOROVOD_SLEEP_INTERVAL=2
export HOROVOD_USE_PRIORITY=0
grank=$PMIX_RANK
lrank=$(($PMIX_RANK%6))

#APP=" nvidia-smi  "

APP=" python -u  ./predict_CosmoFlow.py   --noXterm  --dataSource smt   --design $design  --events $events   "

echo "Full APP=$APP="
echo "shPWD="`pwd`

export PAMI_ENABLE_STRIPING=0

# . . . . .  FIRE THE TASK . . . . . . . .

case ${lrank} in
[0])
export PAMI_IBV_DEVICE_NAME=mlx5_0:1
numactl --physcpubind=0-27 --membind=0 $APP
  ;;
[1])
export PAMI_IBV_DEVICE_NAME=mlx5_1:1
numactl --physcpubind=28-55 --membind=0 $APP
  ;;
[2])
export PAMI_IBV_DEVICE_NAME=mlx5_0:1
numactl --physcpubind=56-83 --membind=0 $APP
  ;;
[3])
export PAMI_IBV_DEVICE_NAME=mlx5_3:1
numactl --physcpubind=88-115 --membind=8 $APP
  ;;
[4])
export PAMI_IBV_DEVICE_NAME=mlx5_2:1
numactl --physcpubind=116-143 --membind=8 $APP
  ;;
[5])
export PAMI_IBV_DEVICE_NAME=mlx5_3:1
numactl --physcpubind=144-171 --membind=8 $APP
  ;;
esac
